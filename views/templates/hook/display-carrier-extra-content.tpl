{*/**
* 2021 VIA.DELIVERY CORPORATION
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author Via.Delivery <info@via.delivery>
* @author IPOL <info@ipol.ru>
* @copyright 2021 VIA.DELIVERY CORPORATION
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/*}
<script>
    document.addEventListener("DOMContentLoaded", function () {
        $('.IViaDel_widget-show-btn').click(function () {
            IViaDel_widgetController.showWidget();
        });
        $('.IViaDel_widget-close-btn').click(function () {
            IViaDel_widgetController.closeWidget();
        });

    });
</script>
<div id="IViaDel_selected-point-info-container"></div>
<button
        type="button"
        class="btn btn-primary mb-1 IViaDel_widget-show-btn"
        rel="IViaDel_popup-container">Select delivery point</button>
<div class="IViaDel_overlay-popup"></div>
<div class="IViaDel_popup" id="IViaDel_popup-container">
    <button
            type="button"
            class="IViaDel_widget-close-btn"
            id="IViaDel_widget-close-btn"
    >
        <svg
                aria-hidden="true"
                focusable="false"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 352 512"
        >
            <path
                    fill="currentColor"
                    d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"
            ></path>
        </svg>
    </button>
    <iframe
            style="height: 100%; width: 100%"
            id="viaDeliveryMapFrame"
            frameborder="0"
            src="{$viaFullMapLink|escape:'htmlall':'UTF-8'}"
    ></iframe>
</div>