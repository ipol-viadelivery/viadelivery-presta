{*
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

<div class="panel">
	<h3><i class="icon icon-truck"></i> {l s='Via.Delivery shipping module' mod='viadelivery'}</h3>
	<img src="{$module_dir|escape:'html':'UTF-8'}/logo.png" id="payment-logo" class="pull-right" />
	<p>
		<strong>{l s='Deliver locally at a fraction of the cost of residential shipping.' mod='viadelivery'}</strong><br />
		{l s='Don\'t forget to configure module settings, before using it.' mod='viadelivery'}
	</p>
</div>

<div class="panel">
	<h3><i class="icon icon-tags"></i> {l s='Support' mod='viadelivery'}</h3>
	<p>
		&raquo; {l s='You can contact us if you have any questions' mod='viadelivery'} :
		<ul>
			<li><a href="https://addons.prestashop.com/en/contact-us?id_product=85627" target="_blank">{l s='Contacts' mod='viadelivery'}</a></li>
		</ul>
	</p>
</div>
