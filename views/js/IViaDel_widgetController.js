/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
let IViaDel_widgetController = {
    ajaxToken: '',

    confirmPointMessageListener: function (event)
    {
        if (event.origin !== 'https://widget.viadelivery.pro'){ //get message not from viadelivery? It's not our message
            return;
        }
        let data = JSON.parse(event.data);
        if (!data.emit || data.emit !== 'via-map') {
            return;
        }

        IViaDel_widgetController.storePvzInfo(data);
        IViaDel_widgetController.updateDeliveryInfoOnPage(data.description, IViaDel_widgetController.constructAddress(data), IViaDel_widgetController.constructDeliveryTime(data.min_days, data.max_days));
        IViaDel_widgetController.closeWidget();
    },

    storePvzInfo: function (viaPointInfoObj)
    {
        $.ajax({
            type: 'POST',
            url: '/modules/viadelivery/views/js/ajax.php',
            data:
                {
                    method: 'DeliveryHandler:setPointInfoFromAjax',
                    data: JSON.stringify(viaPointInfoObj),
                    token: this.ajaxToken
                },
            dataType: 'json'
        })
    },

    showWidget()
    {
        $('#IViaDel_popup-container').show();
        $('.IViaDel_overlay-popup').show();
    },

    closeWidget()
    {
        $('#IViaDel_popup-container').hide();
        $('.IViaDel_overlay-popup').hide();
    },

    updateDeliveryInfoOnPage(pointName, full_address, deliveryTimeDescription) {
        let pointDeliveryInfo = pointName + '<br>' + full_address + '<br>' + deliveryTimeDescription;
        $('#IViaDel_selected-point-info-container').html(pointDeliveryInfo);
    },

    constructDeliveryTime(min_days, max_days) {
        let resultStr = '';
        if (min_days === max_days) {
            resultStr = max_days;
        } else {
            resultStr = min_days + '-' + max_days;
        }
        resultStr += ' day';
        if (max_days !== 1) {
            resultStr += 's';
        }
        return resultStr;
    },
    constructAddress(data) {
        if (data.full_address) {
            return data.full_address;
        }
        let retStr = data.country;
        if (data.city) {
            retStr += ', ' + data.city;
        }
        if (data.street) {
            retStr += ', ' + data.street;
        }
        return retStr;
    }
};

window.addEventListener('message', IViaDel_widgetController.confirmPointMessageListener);