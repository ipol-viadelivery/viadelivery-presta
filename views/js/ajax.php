<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

use Ipol\Viadelivery\DeliveryHandler;
use Ipol\Viadelivery\Option;

require_once(dirname(__FILE__, 5) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.inc.php');
require_once(dirname(__FILE__, 5) . DIRECTORY_SEPARATOR . 'init.php');

$via = new Viadelivery();

$ajaxToken = Tools::getValue('token');
if ($ajaxToken !== hash('sha256', Option::getOption('ajaxToken'))) {
    exit();
}

$method = Tools::getValue('method');
$data = Tools::getValue('data');

switch ($method) {
    case 'DeliveryHandler:setPointInfoFromAjax':
        echo json_encode(DeliveryHandler::getInstance()->setPointInfoFromAjax($data));
        exit();
    case 'test':
        echo json_encode(DeliveryHandler::getInstance()->getSelectedPointId());
        exit();
    default:
        exit();
}
