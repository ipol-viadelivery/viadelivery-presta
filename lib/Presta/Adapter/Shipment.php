<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery\Presta\Adapter;


use Address;
use Cart;
use Country;
use Exception;
use Ipol\Viadelivery\Core\Delivery\Cargo;
use Ipol\Viadelivery\Core\Delivery\CargoCollection;
use Ipol\Viadelivery\Core\Delivery\CargoItem;
use Ipol\Viadelivery\Core\Delivery\Location;
use Ipol\Viadelivery\Core\Entity\Money;
use Ipol\Viadelivery\Core\Entity\Packing\ViaDimMerger;
use Ipol\Viadelivery\Option;
use Ipol\Viadelivery\Presta\Tools\UnitConverter;

class Shipment
{
    /**
     * @param Cart $cart
     * @param Address $prestaAddressObj
     * @param string|null $pointId
     * @return \Ipol\Viadelivery\Core\Delivery\Shipment
     * @throws Exception
     */
    public static function convert(Cart $cart, Address $prestaAddressObj, ?string $pointId = null): \Ipol\Viadelivery\Core\Delivery\Shipment
    {
        $coreShipment = new \Ipol\Viadelivery\Core\Delivery\Shipment();

        $coreShipment->setCargoes(self::makeCargoes($cart));

        if (isset($pointId)) {
            $coreShipment->setTo(new Location('cms'));
            $coreShipment->setPvzIdTo($pointId); //optional - if point id is set, calculation will run for this point, city info will be ignored
        } else {
            $coreShipment->setTo(self::convertLocation($prestaAddressObj));
        }

        $cartPriceFloat = $coreShipment->getCargoes()->getTotalPrice()->getAmount();
        $cartRulesArr = $cart->getCartRules();
        if (is_array($cartRulesArr)) {
            //Presta api for getting cart discounts crashes site on version 1.7.7
            foreach ($cartRulesArr as $discount) {
                $cartPriceFloat -= $discount['reduction_amount'];
                if ($discount['reduction_percent']) {
                    $cartPriceFloat -= $cartPriceFloat * $discount['reduction_percent'] / 100;
                }
            }
            if ($cartPriceFloat < 0) {
                $cartPriceFloat = 0;
            }
            $coreShipment->setField('totalOrderCost', new Money($cartPriceFloat));
        }

        return $coreShipment;
    }

    /**
     * @param Address $prestaAddressObj
     * @return Location
     * @throws Exception
     */
    public static function convertLocation(Address $prestaAddressObj): Location
    {
        $toLocation = new Location('cms');

        $countryIso = Country::getIsoById($prestaAddressObj->id_country);
        $unitedAddressFields = $prestaAddressObj->address1;
        if ($prestaAddressObj->address2) {
            $unitedAddressFields .= ' ' . $prestaAddressObj->address2;
        }

        if (!$prestaAddressObj->city && !$prestaAddressObj->postcode && !$unitedAddressFields) {
            throw new Exception('Not enough address-info to calculate');
        }

        $addressArray = [
            $countryIso,
            $prestaAddressObj->city,
            $unitedAddressFields,
            $prestaAddressObj->postcode
        ];
        if ($prestaAddressObj->city && strpos($unitedAddressFields, $prestaAddressObj->city) !== false) {
            unset($addressArray[1]);
        }
        if ($prestaAddressObj->postcode && strpos($unitedAddressFields, $prestaAddressObj->postcode) !== false) {
            unset($addressArray[3]);
        }
        $addressArray = array_filter($addressArray);

        $addressLine = implode(', ', $addressArray);
        $toLocation->setField('AddressFreeForm', $addressLine);

        return $toLocation;
    }

    /**
     * @param Cart $cart
     * @return CargoCollection
     */
    protected static function makeCargoes(Cart $cart): CargoCollection
    {
        $cart->getTotalWeight();

        $cargoes = new CargoCollection(ViaDimMerger::class); //it's important to use ViaDimMerger - it implements dimensions merging logic, declared by Via in TechRequirements
        $cargo = new Cargo(ViaDimMerger::class); //same as above
        foreach ($cart->getProducts() as $arProduct) {
            $item = new CargoItem();

            $item->setPrice(new Money($arProduct['price'])) //estimated price for insurance
                ->setQuantity($arProduct['quantity'])
                ->setWeight($arProduct['weight'] ? UnitConverter::convertWeightToGram($arProduct['weight']) : Option::getOption('defaultWeight')); //to gram

            if (((int)$arProduct['width']) && ((int)$arProduct['height']) && ((int)$arProduct['depth'])) {
                $item->setWidth(UnitConverter::convertDimensionToMm($arProduct['width']))
                    ->setHeight(UnitConverter::convertDimensionToMm($arProduct['height']))
                    ->setLength(UnitConverter::convertDimensionToMm($arProduct['depth']));
            } else {
                $item->setWidth(Option::getOption('defaultLength'))
                    ->setHeight(Option::getOption('defaultWidth'))
                    ->setLength(Option::getOption('defaultHeight'));
            }
            try {
                $cargo->add($item);
            } catch (Exception $e) {
                continue; //if some trouble with item dimensions occurred - ignore this product (also it's impossible)
            }
        }
        $cargoes->add($cargo);

        return $cargoes;
    }
}