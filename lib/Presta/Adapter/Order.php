<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery\Presta\Adapter;


use Address;
use Currency;
use Customer;
use DateTime;
use Exception;
use Ipol\Viadelivery\Core\Entity\Money;
use Ipol\Viadelivery\Core\Order\Item;
use Ipol\Viadelivery\Core\Order\ItemCollection;
use Ipol\Viadelivery\Core\Order\Payment;
use Ipol\Viadelivery\Core\Order\Receiver;
use Ipol\Viadelivery\Core\Order\ReceiverCollection;
use Ipol\Viadelivery\Core\Tools\DiscountParser;
use Ipol\Viadelivery\Option;
use Ipol\Viadelivery\Presta\Tools\UnitConverter;
use OrderPayment;
use Tools;

class Order
{
    /**
     * @param \Order $psOrder
     * @param bool $decline
     * @return \Ipol\Viadelivery\Core\Order\Order
     * @throws Exception - unsupported currency in order
     */
    public static function convertOrder(\Order $psOrder, bool $decline = false): \Ipol\Viadelivery\Core\Order\Order
    {
        $coreOrder = new \Ipol\Viadelivery\Core\Order\Order();

        $address = new Address($psOrder->id_address_delivery);
        $customer = new Customer($psOrder->id_customer);

        $currencyIso = Currency::getIsoCodeById($psOrder->id_currency);
        if (!in_array($currencyIso, ['RUB', 'USD'])) {
            throw new Exception('Unsupported currency in order!');
        }

        $pointId = $address->other;
        $receiverCollection = new ReceiverCollection();
        $receiverCollection->add(self::convertReceiver($address, $customer));

        $coreItemCollection = self::convertArProductsToItemCollection($psOrder->getProducts(), $currencyIso);

        $orderDiscount = new Money($psOrder->total_discounts_tax_incl, $currencyIso);
        $payment = self::convertPayments($psOrder, $currencyIso, $orderDiscount);

        $discountParser = new DiscountParser($orderDiscount, $coreItemCollection);
        $discountParser->parseDiscountToProducts();

        $coreOrder->setLink($psOrder->id) //order id for inner registration in API and later for order status update
            ->setField('pointUuid', $pointId)
            ->setField('arErrors', [])
            ->setField('shippingCompanyHandle', 'Via.Delivery')
            ->setField('fulfillmentStatus', $decline ? 'declined' : 'accepted')
            ->setNumber($psOrder->reference) // order number in CMS that user will see in ViaDelivery
            ->setItems($coreItemCollection)
            ->setReceivers($receiverCollection)
            ->setPayment($payment);

        return $coreOrder;
    }

    /**
     * @param Address $address
     * @param Customer $customer
     * @return Receiver
     */
    protected static function convertReceiver(Address $address, Customer $customer): Receiver
    {
        $receiver = new Receiver();
        $receiver->setFirstName($address->firstname)
            ->setSecondName($address->lastname)
            ->setEmail($customer->email)
            ->setPhone($address->phone_mobile ?: $address->phone);
        return $receiver;
    }

    /**
     * @param array $arPsProducts
     * @param string $currencyIso
     * @return ItemCollection
     * @throws Exception
     */
    protected static function convertArProductsToItemCollection(array $arPsProducts, string $currencyIso): ItemCollection
    {
        $coreItemCollection = new ItemCollection();
        foreach ($arPsProducts as $cmsProduct) {
            $coreItem = self::convertPrestaProductToCoreItem($cmsProduct, $currencyIso);
            $coreItemCollection->add($coreItem);
        }
        return $coreItemCollection;
    }

    /**
     * @param array $arProduct
     * @param string $currencyIso
     * @return Item
     * @throws Exception
     */
    protected static function convertPrestaProductToCoreItem(array $arProduct, string $currencyIso): Item
    {
        $coreItem = new Item(); //core product
        if (!in_array($currencyIso, ['RUB', 'USD'])) {
            throw new Exception('Unsupported currency in order!');
        }

        $quantity = $arProduct['product_quantity']; //here get real quantity from CMS API
        $price = $arProduct['unit_price_tax_incl'];
        $basePrice = $arProduct['original_product_price'];
        $name = $arProduct['product_name'];
        if (ceil($quantity) != $quantity) { //logic for non-int product quantity like metric or liquid products and so for
            $price = $price * $quantity; //price with discounts TODO check discounts
            $basePrice = $basePrice * $quantity; //price w/o discounts
            $name = $name . ' ' . $quantity;
            $quantity = 1;
        }

        $weight = ((float) $arProduct['weight']) ?
            UnitConverter::convertWeightToGram($arProduct['weight']) :
            Option::getOption('defaultWeight');

        if ((float) $arProduct['depth'] && (float) $arProduct['width'] && (float) $arProduct['height']) {
            $length = UnitConverter::convertDimensionToMm($arProduct['depth']);
            $width = UnitConverter::convertDimensionToMm($arProduct['width']);
            $height = UnitConverter::convertDimensionToMm($arProduct['height']);
        } else {
            $length = Option::getOption('defaultLength');
            $width = Option::getOption('defaultWidth');
            $height = Option::getOption('defaultHeight');
        }

        $coreItem->setPrice(new Money($price, $currencyIso)) //price for one product with all discounts
            ->setField('basePrice', new Money($basePrice, $currencyIso))
            ->setName($name)
            ->setQuantity((int) $quantity)
            ->setWeight($weight) //gram, optional
            ->setLength($length) //mm optional
            ->setWidth($width) //mm optional
            ->setHeight($height) //mm optional
            ->setId($arProduct['product_id']) //product id int in documentation - need to check if it works with strings
            //->setArticul($basketItem->getProductId()) //TODO make Artikul
            ->setBarcode(null) //optional
            ->setVatRate($arProduct['tax_rate']); //int, -1 for no VAT (0 for 0%)
        return $coreItem;
    }

    /**
     * @param \Order $psOrder
     * @param string $orderCurrencyIso
     * @return Payment
     */
    protected static function convertPayments(\Order $psOrder, string $orderCurrencyIso, Money $orderDiscount): Payment
    {
        $payment = new Payment();
        $goods = new Money($psOrder->total_products_wt, $orderCurrencyIso);
        $goods->sub($orderDiscount);
        $payment->setDelivery(new Money($psOrder->total_shipping_tax_incl, $orderCurrencyIso))
            ->setGoods($goods)
            ->setPayed(new Money(0, $orderCurrencyIso));
        if (!$psOrder->hasPayments()) {
            return $payment;
        }

        foreach ($psOrder->getOrderPaymentCollection() as $curPayment) {
            /**@var $curPayment OrderPayment*/
            $curPaymentDateAdd = DateTime::createFromFormat('Y-m-d H:i:s', $curPayment->date_add);
            if ($curPaymentDateAdd && (!isset($mostRecentPayment) || $curPaymentDateAdd > $mostRecentPayment)) {
                $mostRecentPayment = $curPaymentDateAdd;
            }

            $payment->getPayed()->add(self::convertOnePaymentForCurrency($curPayment, $orderCurrencyIso)); //how many was pre-payed online
            $psOrder->getOrderPaymentCollection()->next();
        }
        $payment->setField('datePaid', $mostRecentPayment?? new DateTime());
        return $payment;
    }

    /**
     * @param OrderPayment $curPayment
     * @param string $orderCurrencyIso - one of supported by module currencies
     * @return Money
     */
    protected static function convertOnePaymentForCurrency(OrderPayment $curPayment, string $orderCurrencyIso): Money
    {
        $paymentCurrency = new Currency($curPayment->id_currency);
        $orderCurrency = new Currency(Currency::getIdByIsoCode($orderCurrencyIso));
        $moneyAmount = Tools::convertPriceFull($curPayment->amount, $paymentCurrency, $orderCurrency);
        return new Money($moneyAmount, $orderCurrencyIso);
    }

}