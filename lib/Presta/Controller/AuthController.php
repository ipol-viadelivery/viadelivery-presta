<?php


namespace Ipol\Viadelivery\Presta\Controller;


use Ipol\Viadelivery\Option;

class AuthController extends AbstractController
{
    /**
     * @return bool - true if successfully authorized
     */
    public function checkAuth(): bool
    {
        return $this->application->getUid() && $this->application->getToken();
    }


    /**
     * @param string|null $uid
     * @param string|null $token
     * @return bool - true for successful authorization
     */
    public function login(?string $uid, ?string $token): bool
    {
        $this->application
            ->setUid($uid)
            ->setToken($token);
        return $this->checkAuth();
    }

    /**
     * Drop saved uid and token in module options
     */
    public function logout(): void
    {
        Option::setOption('uid', null);
        Option::setOption('token', null);
    }
}