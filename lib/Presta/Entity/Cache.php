<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery\Presta\Entity;


use Ipol\Viadelivery\Core\Entity\CacheInterface;

/**
 * Class Cache
 * @package Ipol\Viadelivery\Presta\Entity
 */
class Cache implements CacheInterface
{
    /**
     * @var \Cache
     */
    protected $prestaCacheLink;
    /**
     * @var int
     */
    protected $life;
    /**
     * @var string
     */
    protected $path = 'IViaDel_';

    public function __construct()
    {
        $this->prestaCacheLink = \Cache::getInstance();
        $this->life = 3600;
    }

    /**
     * @param string $hash
     * @param mixed $data
     * @return void
     */
    public function setCache($hash, $data)
    {
        $this->prestaCacheLink->set($this->path . $hash, $data, $this->life);
    }

    /**
     * @param string $hash
     * @return bool
     */
    public function checkCache($hash): bool
    {
        if (defined('IViaDel_NOCACHE') && IViaDel_NOCACHE === true) {
            return false;
        } else {
            return $this->prestaCacheLink->exists($this->path . $hash);
        }
    }

    /**
     * @param string $hash
     * @return mixed|null
     */
    public function getCache($hash)
    {
        return ($this->checkCache($hash)) ? $this->GetVars($hash) : null;
    }

    /**
     * @return int
     */
    public function getLife()
    {
        return $this->life;
    }

    /**
     * @param int $life
     * @return $this
     */
    public function setLife($life)
    {
        $this->life = intval($life);

        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    private function GetVars($hash)
    {
        return $this->prestaCacheLink->get($this->path . $hash);
    }
}