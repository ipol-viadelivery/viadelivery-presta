<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery\Admin;


use Configuration;
use Context;
use HelperForm;
use Ipol\Viadelivery\Option;
use Ipol\Viadelivery\Presta\Controller\AuthController;
use Module;
use Tools;

class HelperFormInstance extends HelperForm
{
    /**
     * @var $this
     */
    protected static $_instance;
    protected static $tabNames = [
        'auth' => 'Authorization',
        'dimensionsDef' => 'Default product parameters',
        'unitsConversion' => 'Unit conversion',
        'service' => 'Service'
    ];
    /**
     * @var string
     */
    protected $warningBlockHtml = '';

    public static function getInstance(Module $module, Context $context, $identifier): HelperFormInstance
    {
        if (!self::$_instance) {
            self::$_instance = new self($module, $context, $identifier);
        }

        return self::$_instance;
    }

    public function __construct(Module $module, Context $context, $identifier)
    {
        parent::__construct();
        $this->show_toolbar = false;
        $this->table = 'module';
        $this->module = $module;
        $this->default_form_language = $context->language->id;
        $this->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $this->identifier = $identifier;
        $this->submit_action = 'submitViadeliveryModule';
        $this->currentIndex = $context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $module->name . '&tab_module=' . $module->tab . '&module_name=' . $module->name;
        $this->token = Tools::getAdminTokenLite('AdminModules');
    }

    /**
     * @return string[]
     */
    public function getEmptyRequiredFieldsArr(): array
    {
        $emptyRequiredOptionArr = [];
        foreach (self::$tabNames as $tabName => $tabLabel) {
            foreach (Option::getByGroup($tabName) as $optionName => $option) {
                if (Option::isOptionRequired($optionName) && !$option['value']) {
                    $emptyRequiredOptionArr[] = $option['label'];
                }
            }
        }

        return $emptyRequiredOptionArr;
    }

    public function renderForm(): string
    {
        $this->updateTplVars();
        $renderedContent = '';
        foreach (self::$tabNames as $tabName => $tabLabel) {
            $form = Fieldset::getForm($tabName, $tabLabel);
            $this->submit_action = IPOL_VIADELIVERY_LBL . $tabName;
            if ($tabName === 'auth') {
                $authController = new AuthController();
                $this->submit_action .= ($authController->checkAuth()) ? '_logout' : '_login';
            }
            $renderedContent .= $this->generateForm([$form]);
        }

        $emptyRequiredFieldsArr = $this->getEmptyRequiredFieldsArr();
        if (!empty($emptyRequiredFieldsArr)) {
            $this->context->smarty->assign('empty_option_arr', $emptyRequiredFieldsArr);
            $this->warningBlockHtml .= $this->context->smarty->fetch(
                $this->module->getLocalPath() . 'views/templates/admin/configure-warning-block.tpl'
            );
        }

        return $this->warningBlockHtml . $renderedContent;
    }

    /**
     * Save form data.
     */
    public function postProcess(): void
    {
        if (self::checkRegularSubmit()) {
            $this->saveAllFormFields();
            $this->warningBlockHtml = $this->getSuccessWarn('The settings have been successfully updated.');
            return;
        }
        if (true === Tools::isSubmit(IPOL_VIADELIVERY_LBL . 'auth_login')) {
            $authController = new AuthController();
            $uid = Tools::getValue(IPOL_VIADELIVERY_LBL . 'uid');
            $token = Tools::getValue(IPOL_VIADELIVERY_LBL . 'token');
            $this->saveAllFormFields();
            if (!$authController->login($uid, $token)) {
                $authController->logout();
                $this->warningBlockHtml = $this->getErrMsgWarn(
                    'You should use valid pair of uid and token to log in.'
                );
            } else {
                $this->warningBlockHtml = $this->getSuccessWarn('You have been successfully logged in.');
            }
            return;
        }
        if (true === Tools::isSubmit(IPOL_VIADELIVERY_LBL . 'auth_logout')) {
            $authController = new AuthController();
            $authController->logout();
            $this->warningBlockHtml = $this->getSuccessWarn('You have been successfully logged out.');
        }

    }

    protected function updateTplVars()
    {
        $context = Context::getContext();
        $this->tpl_vars = array(
            'fields_value' => self::getFormValuesArr(),
            'languages' => $context->controller->getLanguages(),
            'id_language' => $context->language->id,
        );
    }

    /**
     * @return array
     */
    protected static function getFormValuesArr(): array
    {
        $formValues = [];
        foreach (self::$tabNames as $tabName => $tabLabel) {
            if ($tabName === 'service' && !Option::getOption('serviceEnable')) {
                continue;
            }
            $formValues = array_merge($formValues, Fieldset::getFormValues($tabName));
        }
        $formValues[IPOL_VIADELIVERY_LBL . 'serviceEnable'] = Option::getOption('serviceEnable');

        return $formValues;
    }

    /**
     * @return bool
     */
    protected static function checkRegularSubmit(): bool
    {
        foreach (self::$tabNames as $tabName => $tabLabel) {
            if ($tabName === 'auth') {
                continue;
            }
            if (true === Tools::isSubmit(IPOL_VIADELIVERY_LBL . $tabName)) {
                return true;
            }
        }
        return false;
    }

    protected function saveAllFormFields(): void
    {
        $formValues = self::getFormValuesArr();
        foreach (array_keys($formValues) as $optionNameWithModulePrefix) {
            $optionName = substr($optionNameWithModulePrefix, strlen(IPOL_VIADELIVERY_LBL));
            $newOptionValue = Tools::getValue($optionNameWithModulePrefix);
            if ($newOptionValue !== false) {
                Option::setOption($optionName, $newOptionValue);
            }
        }
    }

    private function getSuccessWarn(string $blockText = '')
    {
        $this->context->smarty->assign('blockText', $blockText);
        return $this->context->smarty->fetch(
            $this->module->getLocalPath() . 'views/templates/admin/configure-success-block.tpl'
        );
    }

    private function getErrMsgWarn(string $blockText = '')
    {
        $this->context->smarty->assign('blockText', $blockText);
        return $this->context->smarty->fetch(
            $this->module->getLocalPath() . 'views/templates/admin/configure-errmsg-block.tpl'
        );
    }

    /**
     * @return string
     */
    public function getWarningBlockHtml(): string
    {
        return $this->warningBlockHtml;
    }

}