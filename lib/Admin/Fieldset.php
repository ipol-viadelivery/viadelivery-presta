<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery\Admin;


use Ipol\Viadelivery\Option;
use Ipol\Viadelivery\Presta\Controller\AuthController;
use Ipol\Viadelivery\Presta\Entity\TranslatorInstance;
use const IPOL_VIADELIVERY_LBL;

class Fieldset
{
    /**
     * Set values for the inputs.
     * @param string $groupName
     * @return array
     */
    public static function getFormValues(string $groupName): array
    {
        $formValArr = [];

        foreach (Option::getByGroup($groupName) as $optionName => $optionDataArr) {
            $formValArr[IPOL_VIADELIVERY_LBL . $optionName] = $optionDataArr['value'];
        }

        return $formValArr;
    }

    /**
     * Create the structure of your form.
     * @param string $groupName
     * @param string $groupLabel
     * @return array
     */
    public static function getForm(string $groupName, string $groupLabel): array
    {
        switch ($groupName) {
            case 'auth':
                $authController = new AuthController();
                if ($authController->checkAuth()) {
                    return self::getAuthStaticBlock($groupName, $groupLabel);
                } else {
                    return self::getDefaultForm($groupName, $groupLabel, 'Sign In to Activate');
                }
            case 'service':
                return self::getServiceForm();
            default:
                return self::getDefaultForm($groupName, $groupLabel);
        }
    }

    /**
     * @param string $groupLabel
     * @param string $groupName
     * @return array[]
     */
    public static function getDefaultForm(string $groupName, string $groupLabel, $submitButtonText = 'Save'): array
    {
        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => TranslatorInstance::getInstance()->l($groupLabel),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(),
                'submit' => array(
                    'title' => TranslatorInstance::getInstance()->l($submitButtonText),
                ),
            ),
        );

        foreach (Option::getByGroup($groupName) as $optionName => $optionDataArr) {
            $form['form']['input'][] = self::getInputParamsArray($optionName, $optionDataArr);
        }
        return $form;
    }

    protected static function getAuthStaticBlock($groupName, $groupLabel): array
    {
        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => TranslatorInstance::getInstance()->l($groupLabel),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(),
                'submit' => array(
                    'title' => TranslatorInstance::getInstance()->l('Sign Out to Deactivate'),
                ),
            ),
        );

        foreach (Option::getByGroup($groupName) as $optionName => $optionDataArr) {
            $formRow = self::getInputParamsArray($optionName, $optionDataArr);
            $formRow['readonly'] = true;
            $form['form']['input'][] = $formRow;
        }
        return $form;
    }

    protected static function getServiceForm(): array
    {
        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => TranslatorInstance::getInstance()->l('Service'),
                    'icon' => 'icon-cogs',
                ),
                'input' => [
                    self::getInputParamsArray('serviceEnable', Option::getArrOptionByName('serviceEnable'))
                ],
                'submit' => array(
                    'title' => TranslatorInstance::getInstance()->l('Save'),
                ),
            ),
        );

        if (Option::getOption('serviceEnable') == true) {
            foreach (Option::getByGroup('service') as $optionName => $optionDataArr) {
                $form['form']['input'][] = self::getInputParamsArray($optionName, $optionDataArr);
            }
        }
        return $form;
    }

    /**
     * @param $optionName
     * @param $optionDataArr
     * @return array
     */
    protected static function getInputParamsArray($optionName, $optionDataArr): array
    {
        $arInput = [
            'name' => IPOL_VIADELIVERY_LBL . $optionName,
            'label' => TranslatorInstance::getInstance()->l($optionDataArr['label']),
            'required' => Option::isOptionRequired($optionName),
        ];

        switch ($optionDataArr['type']) {
            case 'int':
                $arInput['type'] = 'html';
                $arInput['html_content'] = "<input type=\"number\" min=\"1\" value=\"{$optionDataArr['value']}\" name=\"{$arInput['name']}\" id=\"{$arInput['name']}\">";
                break;
            case 'switch':
                $arInput['type'] = 'switch';
                $arInput['is_bool'] = true;
                $arInput['values'] = array(
                    [
                        'id' => $optionName . '_true',
                        'value' => true,
                    ],
                    [
                        'id' => $optionName . '_false',
                        'value' => false,
                    ]
                );
                break;
            default:
                $arInput['type'] = $optionDataArr['type'];
        }
        if (array_key_exists('hasHint', $optionDataArr) && $optionDataArr['hasHint'] && $optionDataArr['hasHint'] != 'N') {
            $arInput['desc'] = TranslatorInstance::getInstance()->l($optionDataArr['hasHint']);
        }
        return $arInput;
    }
}