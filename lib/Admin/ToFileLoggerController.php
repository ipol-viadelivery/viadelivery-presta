<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery\Admin;


use Ipol\Viadelivery\Api\Logger\FileRoute;
use Ipol\Viadelivery\Api\Logger\Psr\Log\LogLevel;

/**
 * Class ToFileLoggerController
 * @package Ipol\Viadelivery\Admin
 */
class ToFileLoggerController extends \Ipol\Viadelivery\Api\Logger\Logger
{
    /**
     * @var string
     */
    protected $curlTemplate = '{method}' . ' ' . '{process}' . PHP_EOL . '{content}';

    /**
     * ToFileLoggerController constructor.
     * @param string $path - absolute path where to add log-data
     */
    public function __construct(string $path = '')
    {
        if (!$path) {
            $path = $_SERVER['DOCUMENT_ROOT'] . '/ipol_common_log.txt';
        }
        $route = new FileRoute($path);
        $route->enable();
        parent::__construct([$route]);
    }

    /**
     * @param mixed $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message = '', array $context = []): void
    {
        if ($level === LogLevel::DEBUG) {
            parent::log($level, $this->interpolate($this->curlTemplate, $context), []);
        } else {
            parent::log($level, $message, $context);
        }
    }

}