<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery\Admin;


use Ipol\Viadelivery\Api\Logger\FileRoute;
use Ipol\Viadelivery\Api\Logger\Logger;
use Ipol\Viadelivery\Api\Logger\Psr\Log\LogLevel;
use Ipol\Viadelivery\Option;

/**
 * Class BitrixLoggerController
 * @package Ipol\Viadelivery\Admin
 */
class PrestaLoggerController extends Logger
{
    /**
     * @var array
     */
    protected $arrLogged;

    const FILE_FORMAT = '.txt';

    /**
     * BitrixLoggerController constructor.
     * @param string $name - file prefix (name without extension)
     */
    public function __construct(string $name = 'common_log')
    {
        $name = IPOL_VIADELIVERY_LBL . $name . self::FILE_FORMAT;
        $arrOpt = Option::getByGroup('service');

        $this->arrLogged = [];
        foreach ($arrOpt as $optName => $arOptVal) {
            if ((strpos($optName, 'log_') === 0) && ($arOptVal['value'] == true)) {
                $this->arrLogged[] = trim(substr($optName, strlen('log_')));
            }
        }

        $path = self::getPath() . $name;

        $route = new FileRoute($path);
        $route->enable();
        parent::__construct([$route]);
    }

    /**
     * @param mixed $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message = '', array $context = []): void
    {
        if ($level === LogLevel::DEBUG) {
            if (array_key_exists('method', $context) && in_array($context['method'], $this->arrLogged)) {
                parent::log($level, $this->interpolate(self::getCurlTemplate(), $context), []);
            }
        } else {
            parent::log($level, $message, $context);
        }
    }

    /**
     * @return string
     */
    public function getLog(): string
    {
        foreach ($this->routes as $route) {
            return $route->read(); //return first existing, if there is one
        }
        return ''; //empty string in case there is no routs for some reason
    }

    protected static function getCurlTemplate(): string
    {
        return '{method}' . ' ' . '{process}' . PHP_EOL . '{content}';
    }

    /**
     * @return string
     * returns directory with logs
     */
    public static function getPath()
    {
        return _PS_ROOT_DIR_ . self::getRelativePath();
    }

    public static function getRelativePath()
    {
        return DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, ['var', 'logs']) . DIRECTORY_SEPARATOR;
    }


}