<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery;

use Exception;
use Ipol\Viadelivery\Presta\Controller\SendOrderController;
use Order;
use PrestaShopDatabaseException;
use PrestaShopException;
use Viadelivery;

class OrderHandler
{
    public static function sendOrder($orderId, $decline = false)
    {
        new Viadelivery();
        try {
            $psOrder = new Order($orderId);
        } catch (PrestaShopDatabaseException | PrestaShopException $e) {
            return 'Could not load order ' . $orderId;
        }
        try {
            $coreOrder = Presta\Adapter\Order::convertOrder($psOrder, $decline);
        } catch (Exception $e) {
            return 'Order uses unsupported currency.'; //case with unsupported currency
        }
        $controller = new SendOrderController();
        try {
            $result = $controller->sendOrder($coreOrder);
        } catch (\Error | \Exception $e) {
            return 'Critical error. Please, contact support.';
        }
        if ($result->isSuccess()) {
            return true;
        } else {
            return $result->getError();
        }
    }
}
