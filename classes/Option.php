<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery;

use Configuration;
use Error;

class Option
{
    public static function setOption(string $optionName, $optionValue)
    {
        Configuration::updateValue(IPOL_VIADELIVERY_LBL . $optionName, $optionValue);
    }

    public static function getByGroup(string $groupName): array
    {
        $return = [];
        foreach (self::collection() as $optionName => $option) {
            if ($option['group'] === $groupName) {
                $option['value'] = self::getOption($optionName);
                $return[$optionName] = $option;
            }
        }

        return $return;
    }

    public static function getArrOptionByName(string $optionName): array
    {
        $arOption = self::collection()[$optionName];
        $arOption['value'] = self::getOption($optionName);
        return $arOption;
    }

    /**
     * @param string $optionName
     * @return mixed
     */
    public static function getDefault(string $optionName)
    {
        if (array_key_exists($optionName, self::collection())) {
            return self::collection()[$optionName]['default'];
        } else {
            throw new Error('ViaDelivery Error: requested option ' . $optionName . ' does not exist!');
        }
    }

    protected static function collection(): array
    {
        return [
            // auth
            'uid' => [
                'group' => 'auth',
                'label' => 'Uid',
                'hasHint' => false,
                'default' => '',
                'required' => true,
                'type' => 'text'
            ],
            'token' => [
                'group' => 'auth',
                'label' => 'Token',
                'hasHint' => false,
                'default' => '',
                'required' => true,
                'type' => 'text'
            ],
            //dimensionsDef
            'defaultWeight' => [
                'group' => 'dimensionsDef',
                'label' => 'Default Weight (gram)',
                'hasHint' => 'N',
                'default' => 1000,
                'required' => true,
                'type' => 'int'
            ],
            'defaultLength' => [
                'group' => 'dimensionsDef',
                'label' => 'Default Length (mm)',
                'hasHint' => 'N',
                'default' => 100,
                'required' => true,
                'type' => 'int'
            ],
            'defaultWidth' => [
                'group' => 'dimensionsDef',
                'label' => 'Default Width (mm)',
                'hasHint' => 'N',
                'default' => 100,
                'required' => true,
                'type' => 'int'
            ],
            'defaultHeight' => [
                'group' => 'dimensionsDef',
                'label' => 'Default Height (mm)',
                'hasHint' => 'N',
                'default' => 100,
                'required' => true,
                'type' => 'int'
            ],
            //unitsConversion
            'weightConversionCoefficient' => [
                'group' => 'unitsConversion',
                'label' => 'Coefficient for conversion default store weight unit to gram',
                'hasHint' => 'N',
                'default' => 1000,
                'required' => true,
                'type' => 'int'
            ],
            'dimensionConversionCoefficient' => [
                'group' => 'unitsConversion',
                'label' => 'Coefficient for conversion default store dimension unit to mm',
                'hasHint' => 'N',
                'default' => 10,
                'required' => true,
                'type' => 'int'
            ],
            'defaultGoodsVat' => [
                'group' => 'secret',
                'hasHint' => 'Y',
                'default' => 0,
                'type' => 'selectbox',
                'variants' => [0 => "0", 10 => "10", 20 => "20"],
            ],
            //autoSend
            'paymentSendOrderProp' => [
                'group' => 'autoSend',
                'hasHint' => 'Y',
                'default' => false,
                'type' => 'multiselect',
                'multiple' => true,
                'variants' => '\Ipol\Viadelivery\Bitrix\Handler\Statuses::getOrderStatuses',
            ],
            'autoSendMode' => [
                'group' => 'autoSend',
                'hasHint' => 'N',
                'default' => "order",
                'type' => 'selectbox',
                'variants' => ["order" => "getMessage", "shipment" => "getMessage"],
            ],
            'autoSendOrderProp' => [
                'group' => 'autoSend',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'selectbox',
                'variants' => '\Ipol\Viadelivery\Bitrix\Handler\Statuses::getOrderStatuses',
            ],
            'autoSendShipmentProp' => [
                'group' => 'autoSend',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'selectbox',
                'variants' => '\Ipol\Viadelivery\Bitrix\Handler\Statuses::getShipmentStatuses',
            ],
            //serviceEnable
            'serviceEnable' => [
                'label' => 'Service mode',
                'group' => 'notForAutoProcessing',
                'hasHint' => 'Show panel with technical service parameters',
                'default' => false,
                'type' => 'switch',
            ],
            // service
            'paymentSendMode' => [
                'group' => 'outOfService',
                'hasHint' => 'Y',
                'default' => false,
                'type' => 'switch',
            ],
            'paymentSendShipmentProp' => [
                'group' => 'outOfService',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'multiselect',
                'multiple' => true,
                'variants' => '\Ipol\Viadelivery\Bitrix\Handler\Statuses::getShipmentStatuses',
            ],
            'log_createOrder' => [
                'label' => 'Log create order request',
                'group' => 'service',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'switch'
            ],
            'log_getDeliveryInfo' => [
                'label' => 'Log calculation request',
                'group' => 'secret',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'switch'
            ],
            'log_closestPointsByAddress' => [
                'label' => 'Log calculation request',
                'group' => 'service',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'switch'
            ],
            'log_getPointInfo' => [
                'label' => 'Log point info request',
                'group' => 'service',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'switch'
            ],
            'timeout' => [
                'label' => 'curl request timeout (sec)',
                'group' => 'service',
                'hasHint' => 'N',
                'default' => 12,
                'required' => true,
                'type' => 'int'
            ],
            'cacheOff' => [
                'label' => 'Disable cache',
                'group' => 'service',
                'hasHint' => 'Yes - for disabling module inner data cache. ' .
                    'Module uses data cache to decrease amount of requests to ViaDelivery server for calculation.',
                'default' => false,
                'type' => 'switch'
            ],
            'testMode' => [
                'group' => 'secret',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'switch'
            ],
            'allowCustom' => [
                'group' => 'secret',
                'hasHint' => 'N',
                'default' => false,
                'type' => 'switch'
            ],
            'ajaxToken' => [
                'group' => 'secret',
                'hasHint' => 'N',
                'default' => '',
                'type' => 'text'
            ]
        ];
    }

    public static function getOption(string $optionName)
    {
        return Configuration::get(IPOL_VIADELIVERY_LBL . $optionName, null, null, null, self::getDefault($optionName));
    }

    public static function isOptionRequired(string $optionName): bool
    {
        $optionDataArr = self::collection()[$optionName] ?? [];
        return array_key_exists('required', $optionDataArr) ? $optionDataArr['required'] : false;
    }
}
