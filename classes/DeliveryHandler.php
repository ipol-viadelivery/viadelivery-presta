<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Ipol\Viadelivery;

use Address;
use Cart;
use Context;
use Error;
use Exception;
use Ipol\Viadelivery\Api\Entity\Response\GetPointInfo;
use Ipol\Viadelivery\Api\Entity\Response\WidgetPointInfo;
use Ipol\Viadelivery\Presta\Adapter\Shipment;
use Ipol\Viadelivery\Presta\Controller\Calculator;
use Ipol\Viadelivery\Presta\Controller\PointInfo;
use State;

class DeliveryHandler
{
    /**
     * @var self
     */
    protected static $instance;
    /**
     * @var Core\Delivery\Shipment|null
     */
    protected $coreShipment;
    /**
     * @var string|null
     */
    protected $selectedPointId;

    /**
     * DeliveryHandler constructor.
     */
    protected function __construct()
    {
        if ($cartId = Context::getContext()->cart->id) {
            if (Context::getContext()->cookie->{IPOL_VIADELIVERY_LBL . 'selectedPointIdForCart_' . $cartId} !== false) {
                $this->selectedPointId =
                    Context::getContext()->cookie->{IPOL_VIADELIVERY_LBL . 'selectedPointIdForCart_' . $cartId};
            }
        }
    }

    /**
     * @param string $pointInfoJson
     * @return string[]
     */
    public static function setPointInfoFromAjax(string $pointInfoJson): array
    {
        try {
            $objPointInfo = new WidgetPointInfo($pointInfoJson);
            $objPointInfo->setFields($objPointInfo->getDecoded());
        } catch (Api\BadResponseException $e) {
            header('HTTP/1.0 500 Internal Server Error');
            return ['error' => 'Damaged json with point info'];
        }
        try {
            self::getInstance()->setSelectedPointId($objPointInfo->getId());
        } catch (Error $e) {
            header('HTTP/1.0 500 Internal Server Error');
            return ['error' => 'Point id is null'];
        }
        return ['success' => 'success'];
    }

    /**
     * @param $donorAddressId
     * @return Address
     */
    public static function createAddressFromPointInfo($donorAddressId): Address
    {
        $donorAddress = new Address($donorAddressId);

        $pointId = self::getInstance()->getSelectedPointId();
        if (!$pointId) {
            return $donorAddress;
        }
        $controller = new PointInfo();
        $result = $controller->getPointInfoById($pointId)->getResult();
        if (!$result->isSuccess()) {
            return $donorAddress;
        }

        $objPointInfo = $result->getResponse();

        if ($donorAddress->alias === 'ViaDelivery point' &&
            $donorAddress->address1 === self::createAddressString($objPointInfo) &&
            $donorAddress->other === $objPointInfo->getId()
        ) {
            return $donorAddress;
        }

        $pointAddress = new Address();
        $pointAddress->alias = 'ViaDelivery point';
        $pointAddress->id_customer = $donorAddress->id_customer;
        $pointAddress->lastname = $donorAddress->lastname;
        $pointAddress->firstname = $donorAddress->firstname;
        $pointAddress->id_country = $donorAddress->id_country;
        $pointAddress->id_state = State::getIdByIso($objPointInfo->getRegion());
        $pointAddress->city = $objPointInfo->getCity();
        $pointAddress->postcode = $objPointInfo->getZipCode();
        $pointAddress->address1 = self::createAddressString($objPointInfo);
        $pointAddress->phone = $donorAddress->phone;
        $pointAddress->phone_mobile = $donorAddress->phone_mobile;
        $pointAddress->other = $objPointInfo->getId();
        $pointAddress->add();
        return $pointAddress;
    }

    /**
     * @return string|null
     */
    public function getSelectedPointId(): ?string
    {
        return $this->selectedPointId;
    }

    /**
     * @param string $selectedPointId
     */
    public function setSelectedPointId(string $selectedPointId): void
    {
        $this->selectedPointId = $selectedPointId;
        $cartId = Context::getContext()->cart->id;
        Context::getContext()->cookie->{IPOL_VIADELIVERY_LBL . 'selectedPointIdForCart_' . $cartId} = $selectedPointId;
    }

    /**
     * @param GetPointInfo $objPointInfo
     * @return string
     */
    protected static function createAddressString(GetPointInfo $objPointInfo): string
    {
        return implode(
            ', ',
            [
                implode(' ', [$objPointInfo->getBuilding(), $objPointInfo->getStreet()]),
                $objPointInfo->getCity(),
                implode(' ', [$objPointInfo->getRegion(), $objPointInfo->getZipCode()]),
                $objPointInfo->getCountry(),
            ]
        ); //result format: 1440 46th St, Sacramento, CA 95819, USA
    }

    /**
     * @param Cart $cart
     * @return float|false
     */
    public function calculate(Cart $cart)
    {
        if (is_a($cart, Cart::class) && $cart->id_address_delivery) {
            $prestaAddressObj = new Address($cart->id_address_delivery);

            try {
                $this->coreShipment = Shipment::convert($cart, $prestaAddressObj, $this->selectedPointId);
            } catch (Exception $e) {
                return false;
            }
            $calculator = new Calculator();
            if ($this->coreShipment->getPvzIdTo()) {
                $result = $calculator->calculate($this->coreShipment);
                if ($result) {
                    return $result->getDeliveryMinPrice();
                }
            } else {
                $result = $calculator->calculateByNearestPoint($this->coreShipment);
                if ($result) {
                    return (float) $result->getPrice();
                }
            }
        }

        return false; //if anything went wrong
    }

    /**
     * @return self
     */
    public static function getInstance(): DeliveryHandler
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @return Core\Delivery\Shipment|null
     */
    public function getCoreShipment(): ?Core\Delivery\Shipment
    {
        return $this->coreShipment;
    }
}
