<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace Viadelivery\Controller;

use Ipol\Viadelivery\OrderHandler;
use Ipol\Viadelivery\Presta\Entity\TranslatorInstance;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Tools;

class SendOrderController extends FrameworkBundleAdminController
{
    /**
     * route admin_viadelivery_order_bulk_new_action
     * @param $action
     * @return RedirectResponse
     */
    public function sendOrders($action): RedirectResponse
    {
        $arrOrderIds = Tools::getValue('order_orders_bulk');
        foreach ($arrOrderIds as $orderId) {
            $this->handleSendingOrderToViadelivery($orderId, ($action === 'decline'));
        }
        return $this->redirectToRoute('admin_orders_index');
    }

    public function handleSendingOrderToViadelivery(int $orderId, bool $decline = false)
    {
        $translator = TranslatorInstance::getInstance();
        $answer = OrderHandler::sendOrder($orderId, $decline);
        if (true === $answer) {
            if ($decline) {
                $this->addFlash(
                    'success',
                    $translator->l(
                        "Decline request for order $orderId successfully sent to Via.Delivery."
                    )
                );
            } else {
                $this->addFlash(
                    'success',
                    $translator->l("Order $orderId successfully sent to Via.Delivery.")
                );
            }
        } elseif (!empty($answer)) {
            $this->addFlash('error', $translator->l($answer));
        }
    }
}
