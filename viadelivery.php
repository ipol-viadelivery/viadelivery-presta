<?php
/**
 * 2021 VIA.DELIVERY CORPORATION
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author Via.Delivery <info@via.delivery>
 * @copyright 2021 VIA.DELIVERY CORPORATION
 * @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

use Ipol\Viadelivery\Admin\HelperFormInstance;
use Ipol\Viadelivery\DeliveryHandler;
use Ipol\Viadelivery\Option;
use Ipol\Viadelivery\Presta\Controller\WidgetParameterLoader;
use PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type\SubmitBulkAction;

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once dirname(__FILE__) . '/vendor/autoload.php';

class Viadelivery extends CarrierModule
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'viadelivery';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'Via.Delivery';
        $this->need_instance = 1;
        $this->module_key = 'd23dd56e8e6dbb977f2dd153734c89e4';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Via.Delivery');
        $this->description = $this->l('Integration with Via.Delivery for your shop');

        $this->ps_versions_compliancy = array('min' => '1.7.5', 'max' => '1.8');

        if (!defined('IPOL_VIADELIVERY')) {
            define('IPOL_VIADELIVERY', 'ipol.viadelivery');
        }
        if (!defined('IPOL_VIADELIVERY_LBL')) {
            define('IPOL_VIADELIVERY_LBL', 'IViaDel_');
        }
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $carrier = $this->addCarrier();
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRanges($carrier);
        Configuration::updateValue('VIADELIVERY_LIVE_MODE', false);

        Option::setOption('ajaxToken', md5(time()));

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionObjectOrderAddBefore') &&
            $this->registerHook('displayCarrierExtraContent') &&
            $this->registerHook('actionOrderGridDefinitionModifier') &&
            $this->registerHook('actionValidateStepComplete');
    }

    protected function addCarrier()
    {
        $carrier = new Carrier();

        $carrier->name = $this->l('Via.Delivery');
        $carrier->is_module = true;
        $carrier->active = 1;
        $carrier->need_range = 1;
        $carrier->shipping_external = true;
        $carrier->range_behavior = 0;
        $carrier->external_module_name = $this->name;
        $carrier->shipping_method = Carrier::SHIPPING_METHOD_WEIGHT;

        foreach (Language::getLanguages() as $lang) {
            $carrier->delay[$lang['id_lang']] = $this->l('Delivery time depends on selected point');
        }

        if ($carrier->add() == true) {
            @copy(
                dirname(__FILE__) . '/views/img/carrier_image.jpg',
                _PS_SHIP_IMG_DIR_ . '/' . (int)$carrier->id . '.jpg'
            );
            Option::setOption('carrierId', $carrier->id);
            return $carrier;
        }

        return false;
    }

    protected function addZones($carrier)
    {
        $zones = Zone::getZones();

        foreach ($zones as $zone) {
            $carrier->addZone($zone['id_zone']);
        }
    }

    protected function addGroups($carrier)
    {
        $groups_ids = array();
        $groups = Group::getGroups(Context::getContext()->language->id);
        foreach ($groups as $group) {
            $groups_ids[] = $group['id_group'];
        }

        $carrier->setGroups($groups_ids);
    }

    protected function addRanges($carrier)
    {
        $range_weight = new RangeWeight();
        $range_weight->id_carrier = $carrier->id;
        $range_weight->delimiter1 = '0';
        $range_weight->delimiter2 = '10000';
        $range_weight->add();
    }

    public function uninstall()
    {
        Configuration::deleteByName('VIADELIVERY_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $helper = HelperFormInstance::getInstance($this, $this->context, $this->identifier);

        $helper->postProcess();

        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output . $helper->renderForm();
    }


    public function getOrderShippingCost($params, $shipping_cost)
    {
        $this->getPackageShippingCost($params, $shipping_cost);
    }

    public function getPackageShippingCost($params, $shipping_cost, $products = [])
    {
        if (!$this->activationCheck()) {
            return false;
        }
        return DeliveryHandler::getInstance()->calculate($params);
    }

    public function getOrderShippingCostExternal($params)
    {
        return true;
    }

    public function hookDisplayCarrierExtraContent()
    {
        if (!$this->activationCheck()) {
            return false;
        }
        $widgetController = new WidgetParameterLoader();
        $shipment = DeliveryHandler::getInstance()->getCoreShipment();
        $this->context->smarty->assign([
            'viaFullMapLink' => $widgetController->getFullMapLinkForShipment($shipment),
        ]);
        return $this->display($this->name, 'views/templates/hook/display-carrier-extra-content.tpl');
    }

    public function hookActionObjectOrderAddBefore(array $array)
    {
        if (!$this->activationCheck()) {
            return;
        }
        if (!array_key_exists('object', $array) || !is_a($array['object'], Order::class)) {
            return;
        }
        $order = $array['object'];
        $currentCarrier = new Carrier($order->id_carrier);
        if ($currentCarrier->is_module && $currentCarrier->external_module_name === $this->name) {
            $newAddress = DeliveryHandler::createAddressFromPointInfo($order->id_address_delivery);
            $order->id_address_delivery = $newAddress->id;
        }
    }

    public function hookActionValidateStepComplete($params)
    {
        if (!$this->activationCheck()) {
            return;
        }
        $viadeliveryPointId = DeliveryHandler::getInstance()->getSelectedPointId();
        if (is_null($viadeliveryPointId)) {
            $this->context->controller->errors[] = $this->l('Please select a delivery point on map!');
            $params['completed'] = false;
        }
    }

    public function hookActionOrderGridDefinitionModifier($params)
    {
        if (!$this->activationCheck()) {
            return;
        }
        $sendOrderBulk = new SubmitBulkAction('IViaDel_orders_send');
        $sendOrderBulk->setName($this->l('Send selected to Via.Delivery'))
            ->setOptions(
                [
                    'submit_route' => 'admin_viadelivery_order_bulk_new_action',
                    'confirm_message' => $this->l('Send selected orders to Via.Delivery?'),
                    'route_params' => ['action' => 'create'],
                ]
            );
        $declineOrderBulk = (new SubmitBulkAction('IViaDel_orders_decline'))
            ->setName($this->l('Cancel shipping by Via.Delivery for selected orders'))
            ->setOptions(
                [
                    'submit_route' => 'admin_viadelivery_order_bulk_new_action',
                    'confirm_message' => $this->l('Send cancel request for selected orders to Via.Delivery?'),
                    'route_params' => ['action' => 'decline'],
                ]
            );

        // $params['definition'] is instance of \PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinition
        $params['definition']->getBulkActions()
            ->add($sendOrderBulk)
            ->add($declineOrderBulk);
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        if (!$this->activationCheck()) {
            return '';
        }
        if (is_a($this->context->controller, OrderController::class)) {
            $this->context->controller->addCSS($this->_path . 'views/css/map-widget.css');
            $this->context->controller->addJS($this->_path . '/views/js/IViaDel_widgetController.js');
            $this->context->smarty->assign(['ajaxToken' => hash('sha256', Option::getOption('ajaxToken'))]);
            return $this->display($this->name, 'views/templates/front/pass-token-to-checkout.tpl');
        }
        return '';
    }

    /**
     * @return bool - true if module is active and user successfully logged
     */
    private function activationCheck(): bool
    {
        $authController = new \Ipol\Viadelivery\Presta\Controller\AuthController();
        return $authController->checkAuth();
    }
}
