<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'ipol/viadelivery-presta',
  ),
  'versions' => 
  array (
    'ipol/viadelivery-presta' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'ipol/viadelivery.api' => 
    array (
      'pretty_version' => '1.3.6',
      'version' => '1.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b6cf946c2a529148ae44e2245810e18b489e505',
    ),
    'ipol/viadelivery.core' => 
    array (
      'pretty_version' => '1.7.1',
      'version' => '1.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba97b6d8ace4ebd3b9e9c9f617ca7f9a0deb42b9',
    ),
    'ipol/viadelivery.via' => 
    array (
      'pretty_version' => '1.3.8',
      'version' => '1.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b26da6e57820f5042d26ceb3bdbb8b62d50e2a9',
    ),
  ),
);
