<?php


namespace Ipol\Viadelivery\Api;


use Exception;

/**
 * Class ApiLevelException
 * @package Ipol\Viadelivery\Api
 */
class ApiLevelException extends Exception
{
    /**
     * @var string
     */
    protected $url;
    /**
     * @var mixed|null
     */
    protected $request;
    /**
     * @var mixed|null
     */
    protected $answer;

    /**
     * ApiLevelException constructor.
     * @param $message
     * @param int $code
     * @param string $url
     * @param $request
     * @param $answer
     */
    public function __construct($message = false, int $code = 0, string $url = "", $request = null, $answer = null)
    {
        parent::__construct($message, $code);
        $this->url = $url;
        $this->request = $request;
        $this->answer = $answer;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }


    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }


}