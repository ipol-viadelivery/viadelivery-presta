<?php


namespace Ipol\Viadelivery\Api\Methods;


use Ipol\Viadelivery\Api\Adapter\CurlAdapter;
use Ipol\Viadelivery\Api\ApiLevelException;
use Ipol\Viadelivery\Api\BadResponseException;
use Ipol\Viadelivery\Api\Encoder\EncoderInterface;
use Ipol\Viadelivery\Api\Entity\Request\AbstractRequest;
use Ipol\Viadelivery\Api\Entity\Response\AbstractResponse;
use Ipol\Viadelivery\Api\Entity\Response\ErrorResponse;

/**
 * Class GeneralMethod
 * @package Ipol\Viadelivery\Api
 * @subpackage Methods
 * @method AbstractResponse|mixed|ErrorResponse getResponse
 */
class GeneralMethod extends AbstractMethod
{
    /**
     * GeneralMethod constructor.
     * @param AbstractRequest|mixed|null $data
     * @param CurlAdapter $adapter
     * @param string $responseClass
     * @param EncoderInterface|mixed|null $encoder
     * @throws BadResponseException
     */
    public function __construct($data, CurlAdapter $adapter, string $responseClass, $encoder = null)
    {
        parent::__construct($adapter, $encoder);

        if (!is_null($data)) {
            $this->setData($this->getEntityFields($data));
        }

        try {
            $response = new $responseClass($this->request());
            $response->setSuccess(true);
        } catch (ApiLevelException $e) {
            $response = new ErrorResponse(); //TODO refine improve
            $response->setSuccess(false);
        }
        $this->setResponse($this->reEncodeResponse($response));
        $this->setFields();
    }

}