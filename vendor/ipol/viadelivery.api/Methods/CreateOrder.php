<?php


namespace Ipol\Viadelivery\Api\Methods;

use Ipol\Viadelivery\Api\Adapter\CurlAdapter;
use Ipol\Viadelivery\Api\ApiLevelException;
use Ipol\Viadelivery\Api\BadResponseException;
use Ipol\Viadelivery\Api\Entity\Response\CreateOrder as ObjResponse;
use \Ipol\Viadelivery\Api\Entity\Request\CreateOrder as ObjRequest;
use Ipol\Viadelivery\Api\Entity\Response\ErrorResponse;

/**
 * Class CreateOrder
 * @package Ipol\Viadelivery\Api\Methods
 * @method ObjResponse getResponse
 */
class CreateOrder extends AbstractMethod
{
    /**
     * CreateOrder constructor.
     * @param $data ObjRequest
     * @param $adapter CurlAdapter
     * @param string $uid
     * @param string $token
     * @param null $encoder
     * @throws BadResponseException
     */
    public function __construct(
        ObjRequest $data,
        CurlAdapter $adapter,
        string $uid,
        string $token,
        $encoder = null
    ) {
        parent::__construct($adapter, $encoder);

        $this->setDataGet([
            'id' => $uid,
            'sid' => $token,
        ]);
        $this->setDataPost($this->getEntityFields($data));

        try {
            $response = new ObjResponse($this->request());
            $response->setSuccess(true);
        } catch (ApiLevelException $e) {
            $response = new ErrorResponse(); //TODO refine improve
            $response->setSuccess(false);
        }
        $this->setResponse($this->reEncodeResponse($response));
        $this->setFields();
    }

}