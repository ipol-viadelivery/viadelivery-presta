<?php


namespace Ipol\Viadelivery\Api\Methods;

use Ipol\Viadelivery\Api\Adapter\CurlAdapter;
use Ipol\Viadelivery\Api\Entity\Response\GetPointInfo as PointInfo;

class GetPointInfo extends AbstractMethod
{
    protected $method = 'getPointInfo';
    public function __construct($data, CurlAdapter $adapter, $encoder = false)
    {
        parent::__construct($adapter, $encoder);

        $this->setData($this->getEntityFields($data));

        try {
            $response = new PointInfo($this->request());
        }catch(\Exception $e){
            $this->setResponse(false);
            return $this;
        }
        $this->setResponse($this->reEncodeResponse($response));

        $this->setFields();
        return $this;
    }


    /**
     * @return PointInfo
     */
    public function getResponse()
    {
        return parent::getResponse();
    }

}