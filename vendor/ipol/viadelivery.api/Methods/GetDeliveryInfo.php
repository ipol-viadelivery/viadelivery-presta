<?php


namespace Ipol\Viadelivery\Api\Methods;

use Ipol\Viadelivery\Api\Adapter\CurlAdapter;
use Ipol\Viadelivery\Api\Entity\Response\GetDeliveryInfo as DeliveryInfo;


class GetDeliveryInfo extends AbstractMethod
{
    protected $method = 'getDeliveryInfo';
    public function __construct($data, CurlAdapter $adapter, $encoder = false)
    {
        parent::__construct($adapter, $encoder);

        $this->setData($this->getEntityFields($data));

        try {
            $response = new DeliveryInfo($this->request());
            $response->setSuccess(true);
            $this->setResponse($this->reEncodeResponse($response));
            $this->setFields();

        }catch(\Exception $e){
            $this->setResponse(false);
        } finally {
            return $this;
        }
    }


    /**
     * @return DeliveryInfo
     */
    public function getResponse()
    {
        return parent::getResponse();
    }

}