<?php


namespace Ipol\Viadelivery\Api;


use Error;
use Ipol\Viadelivery\Api\Adapter\CurlAdapter;
use Ipol\Viadelivery\Api\Encoder\EncoderInterface;
use Ipol\Viadelivery\Api\Methods\CreateOrder;
use Ipol\Viadelivery\Api\Methods\GeneralMethod;
use Ipol\Viadelivery\Api\Methods\GetFullMapLink;

/**
 * Class Sdk
 * @package Ipol\Viadelivery\Api
 */
class Sdk
{
    /**
     * @var CurlAdapter
     */
    private $adapter;
    /**
     * @var EncoderInterface|null
     */
    private $encoder;
    /**
     * @var array
     */
    protected $map;
    /**
     * @var string
     */
    protected $uid = '';
    /**
     * @var string
     */
    protected $token = '';

    /**
     * SDK constructor.
     * @param CurlAdapter $adapter
     * @param EncoderInterface|null $encoder
     * @param string $uid
     * @param string $token
     * @param string $mode
     * @param bool $custom
     */
    public function __construct(CurlAdapter $adapter, EncoderInterface $encoder = null, string $uid = '', string $token = '',  string $mode = 'API', bool $custom = false)
    {
        $this->adapter = $adapter;
        $this->encoder = $encoder;
        $this->uid = $uid;
        $this->token = $token;

        $this->map = self::constructMap($mode, $custom);

    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $mode
     * @param bool $allowCustom
     * @return array
     */
    protected function constructMap($mode = 'API', $allowCustom = false)
    {
        $arMap = [
            'createOrder' => [
                'API' => 'https://insales.viadelivery.pro/webhook/update',
                'TEST' => 'https://insales.viadelivery.pro/webhook/update',
                'REQUEST_TYPE' => 'POST_GET',
            ],
            'getPointInfo' => [
                'API' => 'https://map-api.viadelivery.pro/point-list',
                'TEST' => 'https://map-api.viadelivery.pro/point-list',
                'REQUEST_TYPE' => 'GET',
            ],
            'getDeliveryInfo' => [
                'API' => 'https://map-api.viadelivery.pro/point-list/preview',
                'TEST' => 'https://map-api.viadelivery.pro/point-list/preview',
                'REQUEST_TYPE' => 'GET',
            ],
            'getDeliveryInfoModeDistance' => [
                'API' => 'https://map-api.viadelivery.pro/point-list/preview', //it's not a mistake
                'TEST' => 'https://map-api.viadelivery.pro/point-list/preview',
                'REQUEST_TYPE' => 'GET',
            ],
            'getFullMapLink' => [
                'API' => 'https://widget.viadelivery.pro/via.maps',
                'TEST' => 'https://widget.viadelivery.pro/via.maps',
                'REQUEST_TYPE' => 'GET',
            ],
            'geoDecode' => [
                'API' => 'https://map-api.viadelivery.pro/geo-decode',
                'TEST' => 'https://map-api.viadelivery.pro/geo-decode',
                'REQUEST_TYPE' => 'GET',
            ],
        ];

        if(defined('IPOL_OZON_CUSTOM_MAP') && is_array(IPOL_OZON_CUSTOM_MAP)) {
            foreach (IPOL_OZON_CUSTOM_MAP as $method => $url) {
                $arMap[$method]['CUSTOM'] = $url;
            }
        }

        if($mode != 'TEST' && $mode != 'API') {
            throw new Error('Unknown Api-map configuring mode');
        }

        $arReturn = array();
        foreach($arMap as $method => $arData)
        {
            if ($allowCustom && isset($arData['CUSTOM'])) {
                $url = $arData['CUSTOM'];
            } else {
                $url = $arData[$mode];
            }

            $arReturn[$method] = array(
                'URL' => $url,
                'REQUEST_TYPE' => $arData['REQUEST_TYPE']
            );
        }
        return $arReturn;
    }

    /**
     * @param string $method name of method in api-map
     */
    protected function configureRequest(string $method)
    {
        if (array_key_exists($method, $this->map)) {
            $url = $this->map[$method]['URL'];
            $type = $this->map[$method]['REQUEST_TYPE'];
        } else {
            throw new Error('requested method "'.$method.'" not found in module map!');
        }

        $this->adapter->setMethod($method);
        $this->adapter->setUrl($url);
        $this->adapter->setRequestType($type);
    }

    /**
     * @param $data
     * @return bool
     */
    public function auth($data)
    {
        if(is_array($data) &&
            isset($data['uid']) &&
            isset($data['token'])
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Entity\Request\CreateOrder $data
     * @return CreateOrder
     * @throws BadResponseException
     */
    public function createOrder(Entity\Request\CreateOrder $data): CreateOrder
    {
        $this->configureRequest(__FUNCTION__);
        return new CreateOrder($data, $this->adapter, $this->uid, $this->token, $this->encoder);
    }

    /**
     * @param Entity\Request\GetPointInfo $data
     * @return GeneralMethod
     * @throws BadResponseException
     */
    public function getPointInfo(Entity\Request\GetPointInfo $data): GeneralMethod
    {
        $this->configureRequest(__FUNCTION__);
        return new GeneralMethod($data, $this->adapter, Entity\Response\GetPointInfo::class, $this->encoder);
    }

    /**
     * @param Entity\Request\GetDeliveryInfo $data
     * @return GeneralMethod
     * @throws BadResponseException
     */
    public function getDeliveryInfo(Entity\Request\GetDeliveryInfo $data): GeneralMethod
    {
        $this->configureRequest(__FUNCTION__);
        return new GeneralMethod($data, $this->adapter, Entity\Response\GetDeliveryInfo::class, $this->encoder);
    }

    /**
     * @param Entity\Request\GetDeliveryInfoModeDistance $data
     * @return GeneralMethod
     * @throws BadResponseException
     */
    public function getDeliveryInfoModeDistance(Entity\Request\GetDeliveryInfoModeDistance $data): GeneralMethod
    {
        $this->configureRequest(__FUNCTION__);
        return new GeneralMethod($data, $this->adapter, Entity\Response\GetDeliveryInfoModeDistance::class, $this->encoder);
    }
    /**
     * @param Entity\Request\WidgetConfig $data
     * @return GetFullMapLink
     */
    public function getFullMapLink(Entity\Request\WidgetConfig $data): GetFullMapLink
    {
        $this->configureRequest(__FUNCTION__);
        return new GetFullMapLink($data, $this->adapter, $this->encoder);
    }

    /**
     * @param Entity\Request\GeoDecode $data
     * @return GeneralMethod
     * @throws BadResponseException
     */
    public function geoDecode(Entity\Request\GeoDecode $data): GeneralMethod
    {
        $this->configureRequest(__FUNCTION__);
        return new GeneralMethod($data, $this->adapter, Entity\Response\GeoDecode::class, $this->encoder);
    }

}