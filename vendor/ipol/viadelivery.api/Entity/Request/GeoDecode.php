<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


class GeoDecode extends AbstractRequest
{
    /**
     * @var string
     */
    protected $address;

    public function __construct(string $address)
    {
        parent::__construct();
        $this->address = $address;
    }

}