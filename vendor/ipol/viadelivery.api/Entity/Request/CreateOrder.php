<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\Client;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\DeliveryInfo;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\OrderLineList;

/**
 * Class CreateOrder
 * @package Ipol\Viadelivery\Api\Entity\Request
 */
class CreateOrder extends AbstractRequest
{
    /**
     * @var string
     */
    protected $id;
    /**
     * @var string
     */
    protected $number;
    /**
     * @var string
     */
    protected $fulfillment_status;
    /**
     * @var string
     */
    protected $financial_status;
    /**
     * @var string
     */
    protected $paid_at;
    /**
     * @var float
     */
    protected $items_price;
    /**
     * @var float
     */
    protected $delivery_price;
    /**
     * @var float
     */
    protected $total_price;
    /**
     * @var string "RUB"|"USD"
     */
    protected $currency_code;
    /**
     * @var Client
     */
    protected $client;
    /**
     * @var OrderLineList
     */
    protected $order_lines;
    /**
     * @var DeliveryInfo
     */
    protected $delivery_info;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param $client
     * @return $this
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return float
     */
    public function getItemsPrice()
    {
        return $this->items_price;
    }

    /**
     * @param $items_price
     * @return $this
     */
    public function setItemsPrice($items_price)
    {
        $this->items_price = $items_price;
        return $this;
    }

    /**
     * @return OrderLineList
     */
    public function getOrderLines()
    {
        return $this->order_lines;
    }

    /**
     * @param $order_lines
     * @return $this
     */
    public function setOrderLines($order_lines)
    {
        $this->order_lines = $order_lines;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * @param $total_price
     * @return $this
     */
    public function setTotalPrice($total_price)
    {
        $this->total_price = $total_price;
        return $this;
    }

    /**
     * @return DeliveryInfo
     */
    public function getDeliveryInfo()
    {
        return $this->delivery_info;
    }

    /**
     * @param $delivery_info
     * @return $this
     */
    public function setDeliveryInfo($delivery_info)
    {
        $this->delivery_info = $delivery_info;
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryPrice()
    {
        return $this->delivery_price;
    }

    /**
     * @param $delivery_price
     * @return $this
     */
    public function setDeliveryPrice($delivery_price)
    {
        $this->delivery_price = $delivery_price;
        return $this;
    }

    /**
     * @return string
     */
    public function getFinancialStatus()
    {
        return $this->financial_status;
    }

    /**
     * @param $financial_status
     * @return $this
     */
    public function setFinancialStatus($financial_status)
    {
        $this->financial_status = $financial_status;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaidAt()
    {
        return $this->paid_at;
    }

    /**
     * @param $paid_at
     * @return $this
     */
    public function setPaidAt($paid_at)
    {
        $this->paid_at = $paid_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getFulfillmentStatus()
    {
        return $this->fulfillment_status;
    }

    /**
     * @param $fulfillment_status
     * @return $this
     */
    public function setFulfillmentStatus($fulfillment_status)
    {
        $this->fulfillment_status = $fulfillment_status;
        return $this;
    }

    /**
     * @return string/null
     */
    public function getCurrencyCode(): ?string
    {
        return $this->currency_code;
    }

    /**
     * @param string|null $currency_code
     * @return CreateOrder
     */
    public function setCurrencyCode(?string $currency_code): CreateOrder
    {
        $this->currency_code = $currency_code;
        return $this;
    }

}