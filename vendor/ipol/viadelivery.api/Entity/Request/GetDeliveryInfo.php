<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


/**
 * Class GetDeliveryInfo
 * @package Ipol\Viadelivery\Api
 * @subpackage Request
 */
class GetDeliveryInfo extends AbstractRequest
{
    /**
     * @var string - shop UUID - documentations marks this optional, but really now its required
     */
    protected $id;
    /**
     * @var int|null gram
     */
    protected $weight;
    /**
     * @var float|null cm
     */
    protected $length;
    /**
     * @var float|null cm
     */
    protected $width;
    /**
     * @var float|null cm
     */
    protected $height;
    /**
     * @var float|null
     */
    protected $order_price;
    /**
     * @var string|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $region;
    /**
     * @var string|null
     */
    protected $country;
    /**
     * @var string|null - POSTAMAT|TOBACCO|RESTAURANT|POINT
     */
    protected $type;
    /**
     * @var string|null - X5|Sberlogistics
     */
    protected $partner;
    /**
     * @var float|null - for coord square
     */
    protected $min_lng;
    /**
     * @var float|null - for coord square
     */
    protected $max_lng;
    /**
     * @var float|null - for coord square
     */
    protected $min_lat;
    /**
     * @var float|null - for coord square
     */
    protected $max_lat;
    /**
     * @var string|null
     */
    protected $point_id;
    /**
     * @var string - Y/N 'Y' - if you want to get result for all region, when there is non result for city
     */
    protected $region_cities;
    /**
     * @var mixed|null - when GET-parameter insales is present, server will try to get other request-info from POST fields, expecting Insales-formatted date there
     */
    protected $insales;
    /**
     * @var string|null
     */
    protected $address;
    /**
     * @var string|null
     */
    protected $zip_code;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return GetDeliveryInfo
     */
    public function setId(string $id): GetDeliveryInfo
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     * @return GetDeliveryInfo
     */
    public function setWeight(?int $weight): GetDeliveryInfo
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     * @param float|null $length
     * @return GetDeliveryInfo
     */
    public function setLength(?float $length): GetDeliveryInfo
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param float|null $width
     * @return GetDeliveryInfo
     */
    public function setWidth(?float $width): GetDeliveryInfo
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param float|null $height
     * @return GetDeliveryInfo
     */
    public function setHeight(?float $height): GetDeliveryInfo
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOrderPrice(): ?float
    {
        return $this->order_price;
    }

    /**
     * @param float|null $order_price
     * @return GetDeliveryInfo
     */
    public function setOrderPrice(?float $order_price): GetDeliveryInfo
    {
        $this->order_price = $order_price;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return GetDeliveryInfo
     */
    public function setCity(?string $city): GetDeliveryInfo
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return GetDeliveryInfo
     */
    public function setRegion(?string $region): GetDeliveryInfo
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return GetDeliveryInfo
     */
    public function setCountry(?string $country): GetDeliveryInfo
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return GetDeliveryInfo
     */
    public function setType(?string $type): GetDeliveryInfo
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartner(): ?string
    {
        return $this->partner;
    }

    /**
     * @param string|null $partner
     * @return GetDeliveryInfo
     */
    public function setPartner(?string $partner): GetDeliveryInfo
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLng(): ?float
    {
        return $this->min_lng;
    }

    /**
     * @param float|null $min_lng
     * @return GetDeliveryInfo
     */
    public function setMinLng(?float $min_lng): GetDeliveryInfo
    {
        $this->min_lng = $min_lng;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLng(): ?float
    {
        return $this->max_lng;
    }

    /**
     * @param float|null $max_lng
     * @return GetDeliveryInfo
     */
    public function setMaxLng(?float $max_lng): GetDeliveryInfo
    {
        $this->max_lng = $max_lng;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLat(): ?float
    {
        return $this->min_lat;
    }

    /**
     * @param float|null $min_lat
     * @return GetDeliveryInfo
     */
    public function setMinLat(?float $min_lat): GetDeliveryInfo
    {
        $this->min_lat = $min_lat;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLat(): ?float
    {
        return $this->max_lat;
    }

    /**
     * @param float|null $max_lat
     * @return GetDeliveryInfo
     */
    public function setMaxLat(?float $max_lat): GetDeliveryInfo
    {
        $this->max_lat = $max_lat;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPointId(): ?string
    {
        return $this->point_id;
    }

    /**
     * @param string|null $point_id
     * @return GetDeliveryInfo
     */
    public function setPointId(?string $point_id): GetDeliveryInfo
    {
        $this->point_id = $point_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegionCities(): string
    {
        return $this->region_cities;
    }

    /**
     * @param string $region_cities
     * @return GetDeliveryInfo
     */
    public function setRegionCities(string $region_cities): GetDeliveryInfo
    {
        $this->region_cities = $region_cities;

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getInsales()
    {
        return $this->insales;
    }

    /**
     * @param mixed|null $insales
     * @return GetDeliveryInfo
     */
    public function setInsales($insales)
    {
        $this->insales = $insales;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return GetDeliveryInfo
     */
    public function setAddress(?string $address): GetDeliveryInfo
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    /**
     * @param string|null $zip_code
     * @return GetDeliveryInfo
     */
    public function setZipCode(?string $zip_code): GetDeliveryInfo
    {
        $this->zip_code = $zip_code;
        return $this;
    }

}