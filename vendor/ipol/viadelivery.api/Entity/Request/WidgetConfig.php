<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


/**
 * Class WidgetConfig
 * @package Ipol\Viadelivery\Api
 */
class WidgetConfig extends AbstractRequest
{
    /**
     * @var string|null
     */
    protected $lang;
    /**
     * @var string
     */
    protected $action = 'true';
    /**
     * @var string|null
     */
    protected $address;
    /**
     * @var float|null
     */
    protected $lat;
    /**
     * @var float|null
     */
    protected $lng;
    /**
     * @var string|null
     */
    protected $pointId;
    /**
     * @var int|null
     */
    protected $zoom;
    /**
     * @var string
     */
    protected $dealerId;
    /**
     * @var float|null
     */
    protected $orderCost;
    /**
     * @var float|null - gram
     */
    protected $orderWeight;
    /**
     * @var float|null - cm
     */
    protected $orderHeight;
    /**
     * @var float|null - cm
     */
    protected $orderLength;
    /**
     * @var float|null - cm
     */
    protected $orderWidth;

    /**
     * @return string
     */
    public function getLang(): ?string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return WidgetConfig
     */
    public function setLang(string $lang): WidgetConfig
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return WidgetConfig
     */
    public function setAction(string $action): WidgetConfig
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return WidgetConfig
     */
    public function setAddress(string $address): WidgetConfig
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @param string $appendix
     * @return $this
     */
    public function appendAddress(string $appendix) : WidgetConfig
    {
        if($appendix)
            $this->address .= " " . $appendix;

        return $this;
    }

    /**
     * @return int
     */
    public function getZoom(): ?int
    {
        return $this->zoom;
    }

    /**
     * @param int $zoom
     * @return WidgetConfig
     */
    public function setZoom(?int $zoom): WidgetConfig
    {
        $this->zoom = $zoom;
        return $this;
    }

    /**
     * @return string
     */
    public function getDealerId(): ?string
    {
        return $this->dealerId;
    }

    /**
     * @param string $dealerId
     * @return WidgetConfig
     */
    public function setDealerId(?string $dealerId): WidgetConfig
    {
        $this->dealerId = $dealerId;
        return $this;
    }

    /**
     * @return float
     */
    public function getOrderCost(): ?float
    {
        return $this->orderCost;
    }

    /**
     * @param float $orderCost
     * @return WidgetConfig
     */
    public function setOrderCost(?float $orderCost): WidgetConfig
    {
        $this->orderCost = $orderCost;
        return $this;
    }

    /**
     * @return float
     */
    public function getOrderWeight(): ?float
    {
        return $this->orderWeight;
    }

    /**
     * @param float|null $orderWeight
     * @return WidgetConfig
     */
    public function setOrderWeight(?float $orderWeight): WidgetConfig
    {
        $this->orderWeight = $orderWeight;
        return $this;
    }

    /**
     * @return float
     */
    public function getOrderHeight(): ?float
    {
        return $this->orderHeight;
    }

    /**
     * @param float|null $orderHeight
     * @return WidgetConfig
     */
    public function setOrderHeight(?float $orderHeight): WidgetConfig
    {
        $this->orderHeight = $orderHeight;
        return $this;
    }

    /**
     * @return float
     */
    public function getOrderLength(): ?float
    {
        return $this->orderLength;
    }

    /**
     * @param float|null $orderLength
     * @return WidgetConfig
     */
    public function setOrderLength(?float $orderLength): WidgetConfig
    {
        $this->orderLength = $orderLength;
        return $this;
    }

    /**
     * @return float
     */
    public function getOrderWidth(): ?float
    {
        return $this->orderWidth;
    }

    /**
     * @param float|null $orderWidth
     * @return WidgetConfig
     */
    public function setOrderWidth(?float $orderWidth): WidgetConfig
    {
        $this->orderWidth = $orderWidth;
        return $this;
    }

    /**
     * @return float
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     * @return WidgetConfig
     */
    public function setLat(?float $lat): WidgetConfig
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float
     */
    public function getLng(): ?float
    {
        return $this->lng;
    }

    /**
     * @param float|null $lng
     * @return WidgetConfig
     */
    public function setLng(?float $lng): WidgetConfig
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * @return string
     */
    public function getPointId(): ?string
    {
        return $this->pointId;
    }

    /**
     * @param string|null $pointId
     * @return WidgetConfig
     */
    public function setPointId(?string $pointId): WidgetConfig
    {
        $this->pointId = $pointId;
        return $this;
    }

}