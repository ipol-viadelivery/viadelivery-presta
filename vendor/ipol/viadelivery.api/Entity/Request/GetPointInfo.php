<?php


namespace Ipol\Viadelivery\Api\Entity\Request;


class GetPointInfo extends AbstractRequest
{
    protected $point_id;
    protected $id;

    /**
     * @return mixed
     */
    public function getPointId()
    {
        return $this->point_id;
    }

    /**
     * @param mixed $point_id
     * @return GetPointInfo
     */
    public function setPointId($point_id)
    {
        $this->point_id = $point_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return GetPointInfo
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
}