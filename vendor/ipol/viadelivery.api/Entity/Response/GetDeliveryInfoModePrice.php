<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


use Ipol\Viadelivery\Api\Entity\Response\Part\GetDeliveryInfoModePrice\PriceList;

/**
 * Class GetDeliveryInfoModeDistance
 * @package Ipol\Viadelivery\Api
 * @subpackage Response
 */
class GetDeliveryInfoModePrice extends AbstractResponse
{
    /**
     * @var PriceList
     */
    protected $priceArray;

    /**
     * @return PriceList
     */
    public function getPriceArray(): PriceList
    {
        return $this->priceArray;
    }

    /**
     * @param array $pointArray
     */
    public function setPriceArray(array $pointArray): void
    {
        $collection = new PriceList();
        $this->priceArray = $collection->fillFromArray($pointArray);
    }

    public function setFields($fields)
    {
        return parent::setFields(['priceArray' => $fields]);
    }

}