<?php

namespace Ipol\Viadelivery\Api\Entity\Response;


/**
 * Class Auth
 * @package Ipol\Viadelivery\API\Entity\Response
 *
 * @var bool $success

 */
class Auth extends AbstractResponse
{
    protected $success;

    /**
     * @return bool
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return Auth
     */
    public function setSuccess($success)
    {
        $this->success = $success;
        return $this;
    }



}