<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


class GetPointInfo extends AbstractResponse
{
    protected $id;
    protected $partner;
    protected $type;
    protected $lat;
    protected $lng;
    protected $description;
    protected $working_time_from;
    protected $working_time_to;
    protected $zip_code;
    protected $street;
    protected $building;
    protected $city;
    protected $city_pref;
    protected $metro;
    protected $region;
    protected $country;
    protected $cash_allowed;
    protected $card_allowed;
    protected $online_allowed;
    protected $return_allowed;
    protected $price;
    protected $currency;
    protected $delivery_time;
    protected $min_days;
    protected $max_days;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return GetPointInfo
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param mixed $partner
     * @return GetPointInfo
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return GetPointInfo
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     * @return GetPointInfo
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     * @return GetPointInfo
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return GetPointInfo
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingTimeFrom()
    {
        return $this->working_time_from;
    }

    /**
     * @param mixed $working_time_from
     * @return GetPointInfo
     */
    public function setWorkingTimeFrom($working_time_from)
    {
        $this->working_time_from = $working_time_from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingTimeTo()
    {
        return $this->working_time_to;
    }

    /**
     * @param mixed $working_time_to
     * @return GetPointInfo
     */
    public function setWorkingTimeTo($working_time_to)
    {
        $this->working_time_to = $working_time_to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param mixed $zip_code
     * @return GetPointInfo
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     * @return GetPointInfo
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param mixed $building
     * @return GetPointInfo
     */
    public function setBuilding($building)
    {
        $this->building = $building;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return GetPointInfo
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCityPref()
    {
        return $this->city_pref;
    }

    /**
     * @param mixed $city_pref
     * @return GetPointInfo
     */
    public function setCityPref($city_pref)
    {
        $this->city_pref = $city_pref;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMetro()
    {
        return $this->metro;
    }

    /**
     * @param mixed $metro
     * @return GetPointInfo
     */
    public function setMetro($metro)
    {
        $this->metro = $metro;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     * @return GetPointInfo
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return GetPointInfo
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCashAllowed()
    {
        return $this->cash_allowed;
    }

    /**
     * @param mixed $cash_allowed
     * @return GetPointInfo
     */
    public function setCashAllowed($cash_allowed)
    {
        $this->cash_allowed = $cash_allowed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardAllowed()
    {
        return $this->card_allowed;
    }

    /**
     * @param mixed $card_allowed
     * @return GetPointInfo
     */
    public function setCardAllowed($card_allowed)
    {
        $this->card_allowed = $card_allowed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOnlineAllowed()
    {
        return $this->online_allowed;
    }

    /**
     * @param mixed $online_allowed
     * @return GetPointInfo
     */
    public function setOnlineAllowed($online_allowed)
    {
        $this->online_allowed = $online_allowed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReturnAllowed()
    {
        return $this->return_allowed;
    }

    /**
     * @param mixed $return_allowed
     * @return GetPointInfo
     */
    public function setReturnAllowed($return_allowed)
    {
        $this->return_allowed = $return_allowed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return GetPointInfo
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return GetPointInfo
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryTime()
    {
        return $this->delivery_time;
    }

    /**
     * @param mixed $delivery_time
     * @return GetPointInfo
     */
    public function setDeliveryTime($delivery_time)
    {
        $this->delivery_time = $delivery_time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinDays()
    {
        return $this->min_days;
    }

    /**
     * @param mixed $min_days
     * @return GetPointInfo
     */
    public function setMinDays($min_days)
    {
        $this->min_days = $min_days;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMaxDays()
    {
        return $this->max_days;
    }

    /**
     * @param mixed $max_days
     * @return GetPointInfo
     */
    public function setMaxDays($max_days)
    {
        $this->max_days = $max_days;
        return $this;
    }

}