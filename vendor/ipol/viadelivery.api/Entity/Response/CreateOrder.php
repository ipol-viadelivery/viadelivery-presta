<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


class CreateOrder extends AbstractResponse
{
    protected $data;
    protected $status;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return CreateOrder
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return CreateOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    
}