<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


/**
 * Class WidgetPointInfo
 * @package Ipol\Viadelivery\Api\Entity\Response
 */

class WidgetPointInfo extends AbstractResponse
{
    /**
     * @var string|null
     */
    protected $id;
    /**
     * @var string|null
     */
    protected $partner;
    /**
     * @var string|null
     */
    protected $type;
    /**
     * @var float|null
     */
    protected $lat;
    /**
     * @var float|null
     */
    protected $lng;
    /**
     * @var string|null
     */
    protected $description;
    /**
     * @var string|null
     */
    protected $working_time_from;
    /**
     * @var string|null
     */
    protected $working_time_to;
    /**
     * @var string|null
     */
    protected $zip_code;
    /**
     * @var string|null
     */
    protected $street;
    /**
     * @var string|null
     */
    protected $building;
    /**
     * @var string|null
     */
    protected $city;
    /**
     * @var string|null
     */
    protected $city_pref;
    /**
     * @var string|null
     */
    protected $metro;
    /**
     * @var string|null
     */
    protected $region;
    /**
     * @var string|null
     */
    protected $country;
    /**
     * @var string|null
     */
    protected $zone;
    /**
     * @var string|null
     */
    protected $full_address;
    /**
     * @var int|null
     */
    protected $retention_time;
    /**
     * @var string|null
     */
    protected $return_allowed;
    /**
     * @var string|null
     */
    protected $status;
    /**
     * @var string|null
     */
    protected $cash_allowed;
    /**
     * @var string|null
     */
    protected $card_allowed;
    /**
     * @var string|null
     */
    protected $online_allowed;
    /**
     * @var string|null
     */
    protected $prepayment_allowed;
    /**
     * @var string|null
     */
    protected $price;
    /**
     * @var string|null
     */
    protected $currency;
    /**
     * @var int|null
     */
    protected $delivery_time;
    /**
     * @var int|null
     */
    protected $min_days;
    /**
     * @var int|null
     */
    protected $max_days;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return WidgetPointInfo
     */
    public function setId(?string $id): WidgetPointInfo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPartner(): ?string
    {
        return $this->partner;
    }

    /**
     * @param string|null $partner
     * @return WidgetPointInfo
     */
    public function setPartner(?string $partner): WidgetPointInfo
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return WidgetPointInfo
     */
    public function setType(?string $type): WidgetPointInfo
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     * @return WidgetPointInfo
     */
    public function setLat(?float $lat): WidgetPointInfo
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLng(): ?float
    {
        return $this->lng;
    }

    /**
     * @param float|null $lng
     * @return WidgetPointInfo
     */
    public function setLng(?float $lng): WidgetPointInfo
    {
        $this->lng = $lng;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return WidgetPointInfo
     */
    public function setDescription(?string $description): WidgetPointInfo
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWorkingTimeFrom(): ?string
    {
        return $this->working_time_from;
    }

    /**
     * @param string|null $working_time_from
     * @return WidgetPointInfo
     */
    public function setWorkingTimeFrom(?string $working_time_from): WidgetPointInfo
    {
        $this->working_time_from = $working_time_from;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWorkingTimeTo(): ?string
    {
        return $this->working_time_to;
    }

    /**
     * @param string|null $working_time_to
     * @return WidgetPointInfo
     */
    public function setWorkingTimeTo(?string $working_time_to): WidgetPointInfo
    {
        $this->working_time_to = $working_time_to;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    /**
     * @param string|null $zip_code
     * @return WidgetPointInfo
     */
    public function setZipCode(?string $zip_code): WidgetPointInfo
    {
        $this->zip_code = $zip_code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return WidgetPointInfo
     */
    public function setStreet(?string $street): WidgetPointInfo
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBuilding(): ?string
    {
        return $this->building;
    }

    /**
     * @param string|null $building
     * @return WidgetPointInfo
     */
    public function setBuilding(?string $building): WidgetPointInfo
    {
        $this->building = $building;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return WidgetPointInfo
     */
    public function setCity(?string $city): WidgetPointInfo
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCityPref(): ?string
    {
        return $this->city_pref;
    }

    /**
     * @param string|null $city_pref
     * @return WidgetPointInfo
     */
    public function setCityPref(?string $city_pref): WidgetPointInfo
    {
        $this->city_pref = $city_pref;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetro(): ?string
    {
        return $this->metro;
    }

    /**
     * @param string|null $metro
     * @return WidgetPointInfo
     */
    public function setMetro(?string $metro): WidgetPointInfo
    {
        $this->metro = $metro;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return WidgetPointInfo
     */
    public function setRegion(?string $region): WidgetPointInfo
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return WidgetPointInfo
     */
    public function setCountry(?string $country): WidgetPointInfo
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZone(): ?string
    {
        return $this->zone;
    }

    /**
     * @param string|null $zone
     * @return WidgetPointInfo
     */
    public function setZone(?string $zone): WidgetPointInfo
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFullAddress(): ?string
    {
        return $this->full_address;
    }

    /**
     * @param string|null $full_address
     * @return WidgetPointInfo
     */
    public function setFullAddress(?string $full_address): WidgetPointInfo
    {
        $this->full_address = $full_address;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRetentionTime(): ?int
    {
        return $this->retention_time;
    }

    /**
     * @param int|null $retention_time
     * @return WidgetPointInfo
     */
    public function setRetentionTime(?int $retention_time): WidgetPointInfo
    {
        $this->retention_time = $retention_time;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReturnAllowed(): ?string
    {
        return $this->return_allowed;
    }

    /**
     * @param string|null $return_allowed
     * @return WidgetPointInfo
     */
    public function setReturnAllowed(?string $return_allowed): WidgetPointInfo
    {
        $this->return_allowed = $return_allowed;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return WidgetPointInfo
     */
    public function setStatus(?string $status): WidgetPointInfo
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCashAllowed(): ?string
    {
        return $this->cash_allowed;
    }

    /**
     * @param string|null $cash_allowed
     * @return WidgetPointInfo
     */
    public function setCashAllowed(?string $cash_allowed): WidgetPointInfo
    {
        $this->cash_allowed = $cash_allowed;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCardAllowed(): ?string
    {
        return $this->card_allowed;
    }

    /**
     * @param string|null $card_allowed
     * @return WidgetPointInfo
     */
    public function setCardAllowed(?string $card_allowed): WidgetPointInfo
    {
        $this->card_allowed = $card_allowed;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOnlineAllowed(): ?string
    {
        return $this->online_allowed;
    }

    /**
     * @param string|null $online_allowed
     * @return WidgetPointInfo
     */
    public function setOnlineAllowed(?string $online_allowed): WidgetPointInfo
    {
        $this->online_allowed = $online_allowed;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrepaymentAllowed(): ?string
    {
        return $this->prepayment_allowed;
    }

    /**
     * @param string|null $prepayment_allowed
     * @return WidgetPointInfo
     */
    public function setPrepaymentAllowed(?string $prepayment_allowed): WidgetPointInfo
    {
        $this->prepayment_allowed = $prepayment_allowed;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string|null $price
     * @return WidgetPointInfo
     */
    public function setPrice(?string $price): WidgetPointInfo
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     * @return WidgetPointInfo
     */
    public function setCurrency(?string $currency): WidgetPointInfo
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDeliveryTime(): ?int
    {
        return $this->delivery_time;
    }

    /**
     * @param int|null $delivery_time
     * @return WidgetPointInfo
     */
    public function setDeliveryTime(?int $delivery_time): WidgetPointInfo
    {
        $this->delivery_time = $delivery_time;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinDays(): ?int
    {
        return $this->min_days;
    }

    /**
     * @param int|null $min_days
     * @return WidgetPointInfo
     */
    public function setMinDays(?int $min_days): WidgetPointInfo
    {
        $this->min_days = $min_days;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxDays(): ?int
    {
        return $this->max_days;
    }

    /**
     * @param int|null $max_days
     * @return WidgetPointInfo
     */
    public function setMaxDays(?int $max_days): WidgetPointInfo
    {
        $this->max_days = $max_days;
        return $this;
    }

}