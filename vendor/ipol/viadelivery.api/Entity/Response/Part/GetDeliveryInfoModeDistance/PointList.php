<?php


namespace Ipol\Viadelivery\Api\Entity\Response\Part\GetDeliveryInfoModeDistance;


use Ipol\Viadelivery\Api\Entity\AbstractCollection;

/**
 * Class PointList
 * @package Ipol\Viadelivery\Api
 * @subpackage Response
 * @method Point getFirst
 * @method Point getNext
 */
class PointList extends AbstractCollection
{
    /**
     * @var Point[]
     */
    protected $Points;

    /**
     * PointList constructor.
     */
    public function __construct()
    {
        parent::__construct('Points');
    }

}