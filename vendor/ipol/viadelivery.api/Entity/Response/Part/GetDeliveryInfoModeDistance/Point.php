<?php


namespace Ipol\Viadelivery\Api\Entity\Response\Part\GetDeliveryInfoModeDistance;


use Ipol\Viadelivery\Api\Entity\AbstractEntity;
use Ipol\Viadelivery\Api\Entity\Response\Part\AbstractResponsePart;

/**
 * Class Point
 * @package Ipol\Viadelivery\Api
 * @subpackage Entity
 */
class Point extends AbstractEntity
{
    use AbstractResponsePart;

    /**
     * @var string|null - "82fd763e-fae5-4e83-bf34-935ea186f749",
     */
    protected $id;
    /**
     * @var string|null - POSTAMAT|TOBACCO|RESTAURANT|POINT,
     */
    protected $type;
    /**
     * @var float|null - 55.82414,
     */
    protected $lat;
    /**
     * @var float|null - 37.626561,
     */
    protected $lng;
    /**
     * @var string|null - "08:00:00",
     */
    protected $working_time_from;
    /**
     * @var string|null - "23:00:00",
     */
    protected $working_time_to;
    /**
     * @var string|null - "Johnny Trading",
     */
    protected $description;
    /**
     * @var string|null - 458 East 4th St, Los Angeles, CA 90013, USA",
     */
    protected $full_address;
    /**
     * @var string|null - "X5",
     */
    protected $partner;
    /**
     * @var string|null - "5.00",
     */
    protected $price;
    /**
     * @var string|null - "USD",
     */
    protected $currency;
    /**
     * @var int|null - 1,
     */
    protected $delivery_time;
    /**
     * @var int|null - 1,
     */
    protected $min_days;
    /**
     * @var int|null - 2,
     */
    protected $max_days;
    /**
     * @var int|null - 789
     */
    protected $distance;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return float|null
     */
    public function getLat(): ?float
    {
        return $this->lat;
    }

    /**
     * @param float|null $lat
     */
    public function setLat(?float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float|null
     */
    public function getLng(): ?float
    {
        return $this->lng;
    }

    /**
     * @param float|null $lng
     */
    public function setLng(?float $lng): void
    {
        $this->lng = $lng;
    }

    /**
     * @return string|null
     */
    public function getWorkingTimeFrom(): ?string
    {
        return $this->working_time_from;
    }

    /**
     * @param string|null $working_time_from
     */
    public function setWorkingTimeFrom(?string $working_time_from): void
    {
        $this->working_time_from = $working_time_from;
    }

    /**
     * @return string|null
     */
    public function getWorkingTimeTo(): ?string
    {
        return $this->working_time_to;
    }

    /**
     * @param string|null $working_time_to
     */
    public function setWorkingTimeTo(?string $working_time_to): void
    {
        $this->working_time_to = $working_time_to;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getFullAddress(): ?string
    {
        return $this->full_address;
    }

    /**
     * @param string|null $full_address
     */
    public function setFullAddress(?string $full_address): void
    {
        $this->full_address = $full_address;
    }

    /**
     * @return string|null
     */
    public function getPartner(): ?string
    {
        return $this->partner;
    }

    /**
     * @param string|null $partner
     */
    public function setPartner(?string $partner): void
    {
        $this->partner = $partner;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string|null $price
     */
    public function setPrice(?string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return int|null
     */
    public function getDeliveryTime(): ?int
    {
        return $this->delivery_time;
    }

    /**
     * @param int|null $delivery_time
     */
    public function setDeliveryTime(?int $delivery_time): void
    {
        $this->delivery_time = $delivery_time;
    }

    /**
     * @return int|null
     */
    public function getMinDays(): ?int
    {
        return $this->min_days;
    }

    /**
     * @param int|null $min_days
     */
    public function setMinDays(?int $min_days): void
    {
        $this->min_days = $min_days;
    }

    /**
     * @return int|null
     */
    public function getMaxDays(): ?int
    {
        return $this->max_days;
    }

    /**
     * @param int|null $max_days
     */
    public function setMaxDays(?int $max_days): void
    {
        $this->max_days = $max_days;
    }

    /**
     * @return int|null
     */
    public function getDistance(): ?int
    {
        return $this->distance;
    }

    /**
     * @param int|null $distance
     */
    public function setDistance(?int $distance): void
    {
        $this->distance = $distance;
    }

}