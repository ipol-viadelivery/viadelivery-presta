<?php


namespace Ipol\Viadelivery\Api\Entity\Response\Part\GetDeliveryInfoModePrice;


use Ipol\Viadelivery\Api\Entity\AbstractCollection;

/**
 * Class PriceList
 * @package Ipol\Viadelivery\Api
 * @subpackage Response
 * @method Price getFirst
 * @method Price getNext
 */
class PriceList extends AbstractCollection
{
    /**
     * @var Price[]
     */
    protected $Prices;

    /**
     * PriceList constructor.
     */
    public function __construct()
    {
        parent::__construct('Prices');
    }

}