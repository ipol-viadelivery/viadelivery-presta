<?php


namespace Ipol\Viadelivery\Api\Entity\Response\Part\GetDeliveryInfoModePrice;


use Ipol\Viadelivery\Api\Entity\AbstractEntity;
use Ipol\Viadelivery\Api\Entity\Response\Part\AbstractResponsePart;

/**
 * Class Price
 * @package Ipol\Viadelivery\Api
 * @subpackage Response
 */
class Price extends AbstractEntity
{
    use AbstractResponsePart;

    /**
     * @var string - "625",
     */
    protected $count;
    /**
     * @var int|null - 1,
     */
    protected $min_days;
    /**
     * @var int|null - 5,
     */
    protected $max_days;
    /**
     * @var string - "5.00"
     */
    protected $price;

    /**
     * @return string
     */
    public function getCount(): string
    {
        return $this->count;
    }

    /**
     * @param string $count
     */
    public function setCount(string $count): void
    {
        $this->count = $count;
    }

    /**
     * @return int|null
     */
    public function getMinDays(): ?int
    {
        return $this->min_days;
    }

    /**
     * @param int|null $min_days
     */
    public function setMinDays(?int $min_days): void
    {
        $this->min_days = $min_days;
    }

    /**
     * @return int|null
     */
    public function getMaxDays(): ?int
    {
        return $this->max_days;
    }

    /**
     * @param int|null $max_days
     */
    public function setMaxDays(?int $max_days): void
    {
        $this->max_days = $max_days;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

}