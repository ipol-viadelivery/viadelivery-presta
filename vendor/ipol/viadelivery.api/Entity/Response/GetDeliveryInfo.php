<?php


namespace Ipol\Viadelivery\Api\Entity\Response;


/**
 * Class GetDeliveryInfo
 * @package Ipol\Viadelivery\Api\Entity\Response
 */
class GetDeliveryInfo extends AbstractResponse
{
    /**
     * @var int
     */
    protected $count = 0;
    /**
     * @var int|null
     */
    protected $min_days;
    /**
     * @var int|null
     */
    protected $max_days;
    /**
     * @var float|null
     */
    protected $min_delivery_price;
    /**
     * @var float|null
     */
    protected $max_delivery_price;

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return GetDeliveryInfo
     */
    public function setCount(int $count): GetDeliveryInfo
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinDays(): ?int
    {
        return $this->min_days;
    }

    /**
     * @param int|null $min_days
     * @return GetDeliveryInfo
     */
    public function setMinDays(?int $min_days): GetDeliveryInfo
    {
        $this->min_days = $min_days;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxDays(): ?int
    {
        return $this->max_days;
    }

    /**
     * @param int|null $max_days
     * @return GetDeliveryInfo
     */
    public function setMaxDays(?int $max_days): GetDeliveryInfo
    {
        $this->max_days = $max_days;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinDeliveryPrice(): ?float
    {
        return $this->min_delivery_price;
    }

    /**
     * @param float|null $min_delivery_price
     * @return GetDeliveryInfo
     */
    public function setMinDeliveryPrice(?float $min_delivery_price): GetDeliveryInfo
    {
        $this->min_delivery_price = $min_delivery_price;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxDeliveryPrice(): ?float
    {
        return $this->max_delivery_price;
    }

    /**
     * @param float|null $max_delivery_price
     * @return GetDeliveryInfo
     */
    public function setMaxDeliveryPrice(?float $max_delivery_price): GetDeliveryInfo
    {
        $this->max_delivery_price = $max_delivery_price;
        return $this;
    }


    
}