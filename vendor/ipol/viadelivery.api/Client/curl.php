<?php


namespace Ipol\Viadelivery\Api\Client;


use Exception;

class curl
{
    /**
     * @var null|resource
     */
    private $client;
    /**
     * @var mixed
     */
    private $answer;
    /**
     * @var null|int
     */
    private $code;
    /**
     * @var string
     */
    private $url = '';

    /**
     * curl constructor.
     * @param string $url
     * @param array $config
     * @throws Exception
     */
    public function __construct($url = false, array $config = [])
    {
        if(!function_exists('curl_init'))
            throw new Exception('No CURL library');
        $this->client = curl_init();
        if($url)
            $this->setUrl($url);
        if($config)
            $this->config($config);
    }

    /**
     * @param string $data
     * @return $this
     */
    public function post(string $data = ''): curl
    {
        curl_setopt($this->client,CURLOPT_POST, TRUE);
        if($data) {
            curl_setopt($this->client, CURLOPT_POSTFIELDS, $data);
        }
        $this->request();

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function get(array $data = []): curl
    {
        if($data){
            if(strpos($this->url,'?') !== false)
                $this->url = substr($this->url,0, strpos($this->url,'?'));
            $this->url .= '?' . http_build_query($data);
        }
        $this->request();

        return $this;
    }

    /**
     * @param string $data
     * @return $this
     */
    public function put(string $data = ''): curl
    {
        curl_setopt($this->client,CURLOPT_CUSTOMREQUEST, 'PUT');
        if($data) {
            curl_setopt($this->client, CURLOPT_POSTFIELDS, $data);
        }
        $this->request();

        return $this;
    }

    /**
     * @return $this
     */
    public function delete(): curl
    {
        curl_setopt($this->client, CURLOPT_CUSTOMREQUEST, "DELETE");

        $this->request();

        return $this;
    }

    /**
     * @param array $args
     * @return $this
     */
    public function config(array $args): curl
    {
        curl_setopt_array($this->client, $args);

        return $this;
    }

    /**
     * @param int $opt
     * @param mixed $val
     * @return $this
     */
    public function setOpt(int $opt, $val): curl
    {
        curl_setopt($this->client, $opt, $val);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @return array
     */
    public function getRequest(): array
    {
        return array('code' => $this->getCode(), 'answer' => $this->getAnswer());
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl(string $url): curl
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return $this
     */
    public function flee(): curl
    {
        if($this->client)
            curl_close($this->client);
        return $this;
    }

    /**
     * @param bool $close
     * @return $this
     */
    private function request(bool $close = true): curl
    {
        curl_setopt($this->client,CURLOPT_URL, $this->url);
        $this->answer = curl_exec($this->client);
        $this->code   = curl_getinfo($this->client,CURLINFO_HTTP_CODE);

        if($close)
            $this->flee();
        return $this;
    }

}