<?php


namespace Ipol\Viadelivery\Via\Entity;


/**
 * Class WidgetConfigResult
 * @package Ipol\Viadelivery\Via
 *
 * @method string getResponse()
 */
class WidgetConfigResult extends AbstractResult
{
    /**
     * @var string
     */
    protected $fullMapLink = '';

    /**
     * @return $this
     */
    public function parseFields(): WidgetConfigResult
    {
        $this->fullMapLink = $this->getResponse();
        return $this;
    }

    /**
     * @return string
     */
    public function getFullMapLink(): string
    {
        return $this->fullMapLink;
    }


}