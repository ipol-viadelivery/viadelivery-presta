<?php


namespace Ipol\Viadelivery\Via\Entity;


use Ipol\Viadelivery\Api\Entity\Response\GetPointInfo;

/**
 * Class PointResult
 * @package Ipol\Viadelivery\Via\Entity
 * @method GetPointInfo|null getResponse()
 */
class PointResult extends AbstractResult
{
    /**
     * @var GetPointInfo|bool
     */
    protected $point_info;
    /**
     * @var string
     */
    protected $maxDays;
    /**
     * @var string
     */
    protected $minDays;
    /**
     * @var string
     */
    protected $id;

    /**
     * @return bool|GetPointInfo
     */
    public function getPointInfo()
    {
        return $this->point_info;
    }

    /**
     * @param bool|GetPointInfo $point_info
     * @return PointResult
     */
    public function setPointInfo($point_info): PointResult
    {
        $this->point_info = $point_info;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaxDays(): string
    {
        return $this->maxDays;
    }

    /**
     * @param string $maxDays
     * @return PointResult
     */
    public function setMaxDays(string $maxDays): PointResult
    {
        $this->maxDays = $maxDays;
        return $this;
    }

    /**
     * @return string
     */
    public function getMinDays(): string
    {
        return $this->minDays;
    }

    /**
     * @param string $minDays
     * @return PointResult
     */
    public function setMinDays(string $minDays): PointResult
    {
        $this->minDays = $minDays;
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return PointResult
     */
    public function setId(string $id): PointResult
    {
        $this->id = $id;
        return $this;
    }

    public function parseFields()
    {
        $this->setId($this->getPointInfo()->getId());
        $this->setMaxDays($this->getPointInfo()->getMaxDays());
        $this->setMinDays($this->getPointInfo()->getMinDays());
        return $this;
    }
}