<?php


namespace Ipol\Viadelivery\Via\Entity;


use Ipol\Viadelivery\Api\Entity\Response\CreateOrder;

class CreateOrderResult extends AbstractResult
{
    protected $Order;

    /**
     * @return CreateOrder
     */
    public function getOrder()
    {
        return $this->Order;
    }

    /**
     * @param mixed $Order
     * @return $this
     */
    public function setOrder($Order)
    {
        $this->Order = $Order;

        return $this;
    }
}