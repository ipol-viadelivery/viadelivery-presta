<?php


namespace Ipol\Viadelivery\Via\Entity;


/**
 * Class CalculatorResult
 * @package Ipol\Viadelivery\Via\Entity
 */
class CalculatorResult extends AbstractResult
{
    /**
     * @var float
     */
    protected $DeliveryMinPrice;
    /**
     * @var float
     */
    protected $DeliveryMaxPrice;
    /**
     * @var int
     */
    protected $TermMin;
    /**
     * @var int
     */
    protected $TermMax;
    /**
     * @var string
     */
    protected $Description;

    public function parseFields()
    {
        if ($this->response->getCount()) {
            $this->setTermMin($this->response->getMinDays())
                ->setTermMax($this->response->getMaxDays())
                ->setDeliveryMinPrice($this->response->getMinDeliveryPrice())
                ->setDeliveryMaxPrice($this->response->getMaxDeliveryPrice());
        } else {
            $this->setSuccess(false);
        }
    }


    /**
     * @return float
     */
    public function getDeliveryMinPrice()
    {
        return $this->DeliveryMinPrice;
    }

    /**
     * @param float $DeliveryMinPrice
     * @return CalculatorResult
     */
    public function setDeliveryMinPrice($DeliveryMinPrice)
    {
        $this->DeliveryMinPrice = $DeliveryMinPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryMaxPrice()
    {
        return $this->DeliveryMaxPrice;
    }

    /**
     * @param float $DeliveryMaxPrice
     * @return CalculatorResult
     */
    public function setDeliveryMaxPrice($DeliveryMaxPrice)
    {
        $this->DeliveryMaxPrice = $DeliveryMaxPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getTermMin()
    {
        return $this->TermMin;
    }

    /**
     * @param int $TermMin
     * @return CalculatorResult
     */
    public function setTermMin($TermMin)
    {
        $this->TermMin = $TermMin;
        return $this;
    }

    /**
     * @return int
     */
    public function getTermMax()
    {
        return $this->TermMax;
    }

    /**
     * @param int $TermMax
     * @return CalculatorResult
     */
    public function setTermMax($TermMax)
    {
        $this->TermMax = $TermMax;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param string $Description
     * @return CalculatorResult
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
        return $this;
    }

}