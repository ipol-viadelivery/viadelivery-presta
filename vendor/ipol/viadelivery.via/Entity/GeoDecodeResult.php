<?php


namespace Ipol\Viadelivery\Via\Entity;


use Ipol\Viadelivery\Api\Entity\Response\GeoDecode;

/**
 * Class GeoDecode
 * @package Ipol\Viadelivery\Via\Entity
 * @method GeoDecode getResponse
 */
class GeoDecodeResult extends AbstractResult
{

}