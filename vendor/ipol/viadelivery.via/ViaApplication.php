<?php


namespace Ipol\Viadelivery\Via;

use Error;
use Exception;
use Ipol\Viadelivery\Api\Adapter\CurlAdapter;
use Ipol\Viadelivery\Api\Encoder\EncoderInterface;
use Ipol\Viadelivery\Api\Logger\Psr\Log\LoggerInterface;
use Ipol\Viadelivery\Api\Sdk;
use Ipol\Viadelivery\Core\Delivery\Shipment;
use Ipol\Viadelivery\Core\Entity\Collection;
use Ipol\Viadelivery\Core\Order\Order;
use Ipol\Viadelivery\Via\Controller\Calculator;
use Ipol\Viadelivery\Via\Controller\ClosestPointsByAddressController;
use Ipol\Viadelivery\Via\Controller\GeoDecode;
use Ipol\Viadelivery\Via\Controller\GetPointInfo;
use Ipol\Viadelivery\Via\Controller\MapConfigController;
use Ipol\Viadelivery\Via\Controller\RequestController;
use Ipol\Viadelivery\Via\Controller\SendOrder;
use Ipol\Viadelivery\Via\Entity\AbstractResult;
use Ipol\Viadelivery\Via\Entity\CalculatorResult;
use Ipol\Viadelivery\Core\Entity\CacheInterface;
use Ipol\Viadelivery\Via\Entity\ClosestPointsByAddressResult;

/**
 * Class ViaApplication
 * @package Ipol\Viadelivery\Via
 */
class ViaApplication
{
    /**
     * @var string
     */
    protected $uid = '';
    /**
     * @var string
     */
    protected $token = '';
    /**
     * @var bool
     */
    protected $isTest = false;
    /**
     * @var bool - true if using custom URL for requests is allowed
     */
    protected $customAllowed = false;
    /**
     * @var integer
     */
    protected $timeout = 10;
    /**
     * @var EncoderInterface|mixed|null
     */
    protected $encoder;
    /**
     * @var CacheInterface|mixed|null - can be set as false in CMS-Controller to disable cache
     */
    protected $cache;
    /**
     * @var LoggerInterface|mixed|null
     */
    protected $logger;
    /**
     * @var array
     * saves results of calculation via hash
     */
    protected $abyss;
    /**
     * @var bool
     * set - data won't get into the abyss
     */
    protected $blockAbyss = false;
    /**
     * @var string
     */
    protected $hash;
    /**
     * @var string
     * shows how was made last request: via cache, taken from abyss or by actual request to server
     */
    protected $lastRequestType = '';
    /**
     * @var Collection empty if no errors occurred, error-info otherwise
     */
    protected $errorCollection = false;
    /**
     * @deprecated
     * @var mixed false if no errors occurred, error-info otherwise
     */
    protected $lastError = false;

    /**
     * ViaApplication constructor.
     * @param string $uid
     * @param string $token
     * @param false $isTest
     * @param int $timeout
     * @param EncoderInterface|mixed|null $encoder
     * @param CacheInterface|mixed|null $cache
     * @param LoggerInterface|mixed|null $logger
     */
    public function __construct(
        string $uid = '',
        string $token = '',
        bool $isTest = false,
        int $timeout = 10,
        $encoder = null,
        $cache = null,
        $logger = null
    ) {
        $this->setUid($uid)
            ->setToken($token)
            ->setIsTest($isTest)
            ->setTimeout($timeout)
            ->setEncoder($encoder)
            ->setCache($cache)
            ->setLogger($logger);

        $this->errorCollection = new Collection('errors');

        $this->abyss = array();
    }

    /**
     * @param string $type
     * @param string $lang
     * @return string
     */
    public function getWidgetUrl(string $type, string $lang = 'en'): string
    {
        switch ($type) {
            case 'cities':
                $url = 'https://widget.viadelivery.pro/via.charts/cities.html';
                break;
            case 'payment-types':
                $url = 'https://widget.viadelivery.pro/via.charts/payment-types.html';
                break;
            case 'unclaimed':
                $url = 'https://widget.viadelivery.pro/via.charts/unclaimed.html';
                break;
            case 'shipments-per-day':
                $url = 'https://widget.viadelivery.pro/via.charts/shipments-per-day.html';
                break;
            default:
                throw new Error('Wrong widget type for ' . __METHOD__);
        }
        if ($url) {
            return $url.'?id='.$this->getUid().'&locale='.$lang;
        } else {
            return '';
        }
    }

    /**
     * Calculate delivery price and terms for Core shipment
     * @param Shipment $order
     * @param bool $nocache
     * @return CalculatorResult
     */
    public function calculate(Shipment $order, bool $nocache = false): CalculatorResult
    {
        $controller = new Calculator(new Entity\CalculatorResult(), $order);
        $controller->setSdkMethodName('getDeliveryInfo');
        return $this->genericCall($controller, !$nocache);
    }

    /**
     * @param Shipment $shipment
     * @param string|null $address
     * @param int|null $groupLimit
     * @param bool $useCache
     * @return ClosestPointsByAddressResult
     */
    public function closestPointsByAddress(Shipment $shipment, ?string $address = null, ?int $groupLimit = null, bool $useCache = true): ClosestPointsByAddressResult
    {
        $controller = new ClosestPointsByAddressController(new Entity\ClosestPointsByAddressResult(), $shipment, $address, $groupLimit);
        $controller->setSdkMethodName('getDeliveryInfoModeDistance');
        return $this->genericCall($controller, $useCache);
    }

    /**
     * Get Widget config object (for bitrix widget and alike)
     * @param Shipment|null $shipment
     * @param string $lang
     * @return Entity\WidgetConfigResult
     */
    public function getMapConfig(?Shipment $shipment, string $lang = 'en'): Entity\WidgetConfigResult
    {
        $controller = new MapConfigController(new Entity\WidgetConfigResult(), $shipment, $lang);
        $controller->setSdkMethodName('getFullMapLink');
        return $this->genericCall($controller, true);
    }

    /**
     * Send order info to ViaDelivery
     * @param Order $order
     * @return Entity\CreateOrderResult
     */
    public function sendOrder(Order $order): Entity\CreateOrderResult
    {
        $this->lastRequestType = 'direct';

        $controller = new SendOrder();
        try {
            $this->configureController($controller);
        } catch (Exception $e) {
            $this->lastError = $e->getMessage().' ('.$e->getCode().')';
            $failRes = new Entity\CreateOrderResult();
            $failRes->setSuccess(false);

            return $failRes;
        }

        return $controller->setOrder($order)
            ->convert()
            ->send();
    }

    /**
     * @param $pointId
     * @return Entity\PointResult
     */
    public function getPointInfo($pointId): Entity\PointResult //TODO: refactor
    {
        $this->setHash('info'.$pointId);

        if ($this->checkAbyss()) {
            $this->lastRequestType = 'abyss';
            $req = $this->abyss[$this->getHash()];
        } else {
            $this->lastRequestType = 'direct';
            $controller = new GetPointInfo($pointId);

            try {
                $this->configureController($controller);
                $req = $controller->execute();

            } catch (Exception $e) {
                $this->lastError = $e->getMessage().' ('.$e->getCode().')';
                $failRes = new Entity\PointResult();
                $failRes->setSuccess(false);

                return $failRes;
            }
        }
        $this->toAbyss($req);

        return $req;
    }

    /**
     * @param string $address
     * @return Entity\GeoDecodeResult
     */
    public function geoDecode(string $address) : Entity\GeoDecodeResult
    {
        $controller = new GeoDecode(new Entity\GeoDecodeResult(), $address);
        $controller->setSdkMethodName(__FUNCTION__);
        return $this->genericCall($controller, true);
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return ViaApplication
     */
    public function setUid(string $uid): ViaApplication
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setToken(string $token): ViaApplication
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsTest(): bool
    {
        return $this->isTest;
    }

    /**
     * @param mixed $isTest
     * @return $this
     */
    public function setIsTest($isTest): ViaApplication
    {
        $this->isTest = $isTest;

        return $this;
    }

    /**
     * @return $this
     */
    public function allowCustom(): ViaApplication
    {
        $this->customAllowed = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function disallowCustom(): ViaApplication
    {
        $this->customAllowed = false;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     * @return $this
     */
    public function setTimeout(int $timeout): ViaApplication
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEncoder()
    {
        return $this->encoder;
    }

    /**
     * @param mixed $encoder
     * @return $this
     */
    public function setEncoder($encoder): ViaApplication
    {
        $this->encoder = $encoder;

        return $this;
    }

    /**
     * @return CacheInterface|mixed|null
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param CacheInterface|mixed|null $cache
     * @return $this
     */
    public function setCache($cache): ViaApplication
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * @param mixed $data
     * @param string $hash
     * @return $this
     */
    public function toCache($data, string $hash = ''): ViaApplication
    {
        if (!$hash) {
            $hash = $this->getHash();
        }

        if (!isset($hash) || is_null($this->getCache())) {
            return $this;
        }

        $this->getCache()->setCache($hash, $data);

        return $this;
    }

    /**
     * @return LoggerInterface|mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface|mixed $logger
     * @return $this
     */
    public function setLogger($logger): ViaApplication
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * @param mixed $val
     * @param string $hash
     * @return $this
     * returns saved request
     */
    public function toAbyss($val, string $hash = ''): ViaApplication
    {
        $hash = ($hash) ?: $this->getHash();

        if (!$this->blockAbyss &&
            $hash
        ) {
            $this->abyss[$hash] = $val;
        }

        return $this;
    }

    /**
     * @param bool|string $hash
     * @return bool|mixed
     * checks whether same request was already done
     */
    public function checkAbyss($hash = false)
    {
        $hash = ($hash) ?: $this->getHash();

        if (!$this->blockAbyss &&
            $hash &&
            array_key_exists($hash, $this->abyss)
        ) {
            return $this->abyss[$hash];
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isAbyssLocked(): bool
    {
        return $this->blockAbyss;
    }

    /**
     * @param bool $blockAbyss
     * @return $this
     */
    public function setAbyssLock(bool $blockAbyss): ViaApplication
    {
        $this->blockAbyss = $blockAbyss;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     * @return $this
     */
    public function setHash(string $hash): ViaApplication
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastRequestType(): string
    {
        return $this->lastRequestType;
    }

    /**
     * @return $this
     * resets hash and abyss
     */
    public function flee(): ViaApplication
    {
        $this->setHash('')
            ->setAbyssLock(false);

        $this->lastRequestType = '';

        return $this;
    }

    /**
     * @param mixed $error - throwable (Exceptions)
     * @return $this
     */
    protected function addError($error): ViaApplication
    {
        if (!$this->errorCollection) {
            $this->errorCollection = new Collection('errors');
        }

        $this->errorCollection->add($error);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getErrorCollection()
    {
        return $this->errorCollection;
    }

    /**
     * @param RequestController|mixed $controller
     * sets sdk
     * @throws Exception
     */
    protected function configureController($controller)
    {
        $controller->setSdk($this->getSdk());
    }

    /**
     * @return Sdk
     * get the sdk-controller
     * ! timeout sets only here: later it wouldn't be changed !
     * @throws Exception
     */
    public function getSdk(): Sdk
    {
        $mode = $this->getIsTest() ? 'TEST' : 'API';
        $adapter = new CurlAdapter($this->getTimeout());
        if ($this->getLogger()) {
            $adapter->setLog($this->getLogger());
        }

        return new Sdk($adapter, $this->getEncoder(), $this->getUid(), $this->getToken(), $mode, $this->customAllowed);
    }

    /**
     * @deprecated
     * @return ViaApplication
     */
    public function unlockAbyss(): ViaApplication
    {
        $this->blockAbyss = false;

        return $this;
    }

    /**
     * @return $this
     * @deprecated
     */
    public function lockAbyss(): ViaApplication
    {
        $this->blockAbyss = true;

        return $this;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * Inner automated method for common steps in request, for DRY principle
     * @param $controller
     * @param bool $useCache
     * @param int $cacheTTL
     * @return AbstractResult|mixed
     */
    private function genericCall($controller, bool $useCache = false, int $cacheTTL = 3600)
    {
        $resultObj = $controller->getResultObject();
        $this->setHash($controller->getSelfHash());
        if ($this->checkAbyss()) {
            $this->lastRequestType = 'abyss';

            return $this->abyss[$this->getHash()];
        } else {
            if ($useCache && $this->getCache() && $this->getCache()->setLife($cacheTTL)->checkCache($this->getHash())) {
                $this->lastRequestType = 'cache';

                return $this->getCache()->getCache($this->getHash());
            } else {
                $this->lastRequestType = 'direct';

                try {
                    $this->configureController($controller);
                } catch (Exception $e) {
                    $this->addError($e);

                    return $resultObj;
                }
                $controller->convert()
                    ->execute();

                if ($resultObj->getError()) {
                    $this->addError($resultObj->getError());
                } else {
                    $this->toAbyss($resultObj);
                    if ($useCache) {
                        $this->toCache($resultObj);
                    }
                }
            }
        }

        return $resultObj;
    }

}