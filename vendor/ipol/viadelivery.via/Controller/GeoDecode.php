<?php


namespace Ipol\Viadelivery\Via\Controller;


use Ipol\Viadelivery\Api\Entity\Request\GeoDecode as RequestObj;

class GeoDecode extends AutomatedCommonRequest
{
    public function __construct($resultObj, $address)
    {
        parent::__construct($resultObj);
        $this->requestObj = new RequestObj($address);
    }

}