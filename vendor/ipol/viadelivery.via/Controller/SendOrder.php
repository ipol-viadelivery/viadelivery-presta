<?php


namespace Ipol\Viadelivery\Via\Controller;


use DateTime;
use Ipol\Viadelivery\Api\Entity\Request\CreateOrder;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\Client;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\DeliveryInfo;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\OrderLine;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\OrderLineList;
use Ipol\Viadelivery\Core\Entity\Money;
use Ipol\Viadelivery\Core\Order\Order;
use Ipol\Viadelivery\Via\Entity\CreateOrderResult;

/**
 * Class SendOrder
 * @package Ipol\Viadelivery\Via\Controller
 */
class SendOrder extends RequestController
{
    /**
     * @var Order
     */
    protected $order;

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return SendOrder
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return $this
     * converts Core order to request-object for Api
     */
    public function convert()
    {
        $order = $this->getOrder();

        $data = new CreateOrder();

        $buyer = new Client();
        $buyer->setName($order->getReceivers()->getFirst()->getFullName())
            ->setEmail($order->getReceivers()->getFirst()->getEmail())
            ->setPhone($order->getReceivers()->getFirst()->getPhone());

        $deliveryInfo = new DeliveryInfo();
        $deliveryInfo->setPrice($order->getPayment()->getDelivery()->getAmount())
            ->setOutlet($order->getField('pointUuid'))
            ->setErrors($order->getField('arErrors'))
            ->setShippingCompanyHandle($order->getField('shippingCompanyHandle'));

        $obProductList = new OrderLineList();
        $itemCollection = $order->getItems();
        while ($item = $itemCollection->getNext())
        {
            $obProduct = new OrderLine();
            $obProduct->setVat($item->getVatRate())
                ->setTitle($item->getName())
                ->setWeight($item->getWeight() / 1000) //Core gram -> API kg
                ->setFullSalePrice($item->getPrice()->getAmount())
                //->setTotalPrice(Money::multiply($item->getField('basePrice'), $item->getQuantity())->getAmount()) deprecated (or not)
                ->setQuantity($item->getQuantity())
                ->setFullTotalPrice(Money::multiply($item->getPrice(), $item->getQuantity())->getAmount())
                ->setProductId($item->getId())
                //->setVariantId($item->getArticul())
                ->setBarcode($item->getBarcode());
            if(!is_null($item->getWidth()) && !is_null($item->getHeight()) && !is_null($item->getLength())) {
                $obProduct->setDimensions(($item->getWidth() / 10).'x'.($item->getHeight() / 10).'x'.($item->getLength() / 10)); //Core mm -> API formatted string with cm;
            }

            $obProductList->add($obProduct);
        }

        $data->setId($order->getLink())
            ->setClient($buyer)
            ->setNumber($order->getNumber())
            ->setItemsPrice($order->getPayment()->getGoods()->getAmount())
            ->setOrderLines($obProductList)
            ->setTotalPrice(Money::sum($order->getPayment()->getGoods(),
                $order->getPayment()->getDelivery())->getAmount())
            ->setDeliveryInfo($deliveryInfo)
            ->setDeliveryPrice($order->getPayment()->getDelivery()->getAmount())
            ->setCurrencyCode($order->getItems()->getFirst()->getPrice()->getCurrency())
            ->setFulfillmentStatus($order->getField('fulfillmentStatus'));
        if ($order->getPayment()->getNominalPrice()->getAmount() > 0) {
            $data->setFinancialStatus('pending');
        } else {
            /**@var DateTime $datePaid*/
            $datePaid = $order->getPayment()->getField('datePaid');
            $data->setPaidAt($datePaid->format('Y-m-d\TH:i:s.vP'))
                ->setFinancialStatus('paid');
        }

        $this->setRequestObj($data);

        return $this;
    }

    /**
     * @return CreateOrderResult
     */
    public function send()
    {
        $result = new CreateOrderResult();

        try {
            if ($this->getRequestObj()) {
                $data = $this->getRequestObj();
            } else {
                throw new \Exception('No order to send');
            }

            $request = $this->getSdk()
                ->createOrder($data);

            if($request->getResponse()->getSuccess())
            {
                $result->setOrder($request->getResponse())
                ->setSuccess(true);
            }
            else
            {
                $result->setSuccess(false);
                throw new \Exception('Unsuccessful request');
            }
        } catch (\Exception $e) {
            $result->setSuccess(false)
                ->setError($e->getMessage());
        } finally {
            return $result;
        }
    }

}