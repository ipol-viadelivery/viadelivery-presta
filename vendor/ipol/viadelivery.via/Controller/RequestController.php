<?php


namespace Ipol\Viadelivery\Via\Controller;


use Ipol\Viadelivery\Api\Entity\Request\AbstractRequest;
use Ipol\Viadelivery\Api\Sdk;
use Ipol\Viadelivery\Via\AppLevelException;

/**
 * Class RequestController
 * @package Ipol\Viadelivery\Via
 * @subpackage Controller
 */
abstract class RequestController
{
    /**
     * @var Sdk
     */
    protected $Sdk;
    /**
     * @var mixed|AbstractRequest
     */
    protected $requestObj;
    /**
     * @var string
     */
    protected $sdkMethodName;

    /**
     * @return $this|mixed
     */
    public function convert()
    {
        return $this;
    }
    /**
     * @return mixed
     */
    public function getRequestObj()
    {
        return $this->requestObj;
    }

    /**
     * @param mixed|AbstractRequest $requestObj
     * @return $this|mixed
     */
    public function setRequestObj($requestObj)
    {
        $this->requestObj = $requestObj;
        return $this;
    }

    /**
     * @return Sdk
     * @throws AppLevelException
     */
    public function getSdk(): ?Sdk
    {
        if(!$this->Sdk)
            throw new AppLevelException('Accessing Sdk before setting and configuring it');
        return $this->Sdk;
    }

    /**
     * @param Sdk $Sdk
     * @return $this|mixed
     */
    public function setSdk(Sdk $Sdk)
    {
        $this->Sdk = $Sdk;

        return $this;
    }
    /**
     * @return string
     */
    public function getSdkMethodName(): string
    {
        return $this->sdkMethodName;
    }

    /**
     * @param string $sdkMethodName
     * @return $this|mixed
     */
    public function setSdkMethodName(string $sdkMethodName)
    {
        $this->sdkMethodName = $sdkMethodName;
        return $this;
    }

}