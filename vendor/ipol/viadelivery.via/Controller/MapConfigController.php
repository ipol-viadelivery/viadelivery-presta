<?php


namespace Ipol\Viadelivery\Via\Controller;


use Ipol\Viadelivery\Api\Entity\Request\WidgetConfig as RequestObj;
use Ipol\Viadelivery\Core\Delivery\Shipment;
use Ipol\Viadelivery\Core\Entity\Money;
use Ipol\Viadelivery\Via\AppLevelException;
use Ipol\Viadelivery\Via\Entity\WidgetConfigResult as ResultObj;

class MapConfigController extends AutomatedCommonRequest
{
    /**
     * @var Shipment|null
     */
    protected $shipment;

    public function __construct(ResultObj $resultObj, ?Shipment $shipment, string $lang = 'en')
    {
        parent::__construct($resultObj);
        $this->shipment = $shipment;
        $this->requestObj = new RequestObj();
        $this->requestObj->setLang($lang);
    }

    /**
     * @return $this
     */
    public function convert(): MapConfigController
    {
        $obMapParams = $this->requestObj;

        try {
            $obMapParams->setDealerId((string)$this->getSdk()->getUid());
        } catch (AppLevelException $e) {
            //support case when we try to get map with no uid
        }

        if ($this->getShipment()) {
            $shipment = $this->getShipment();
            
            $dimensions = $shipment->getCargoes()->getTotalDimensions();

            if (is_a($shipment->getField('totalOrderCost'), Money::class)) {
                $orderPrice = $shipment->getField('totalOrderCost');
            } else {
                $orderPrice = $shipment->getCargoes()->getTotalPrice();
            }

            $obMapParams->setOrderCost($orderPrice->getAmount())
                ->setOrderWeight($shipment->getCargoes()->getTotalWeight()) //gram stays gram
                ->setOrderLength((int)$dimensions['L'] / 10) //Core mm -> API cm
                ->setOrderWidth((int)$dimensions['W'] / 10) //Core mm -> API cm
                ->setOrderHeight((int)$dimensions['H'] / 10); //Core mm -> API cm
            if ($shipment->getPvzIdTo()) {
                $obMapParams->setPointId($shipment->getPvzIdTo());
            } elseif ($shipment->getTo()) {
                if ($shipment->getTo()->getField('AddressFreeForm')) {
                    $obMapParams->setAddress($shipment->getTo()->getField('AddressFreeForm'));
                } else {
                    $obMapParams->setAddress((string)$shipment->getTo()->getName())
                        ->appendAddress((string)$shipment->getTo()->getRegion())
                        ->appendAddress((string)$shipment->getTo()->getCountry());
                }
            }
        }

        $this->setRequestObj($obMapParams);
        return $this;
    }

    public function execute(): ResultObj
    {
        $result = $this->getResultObject();

        $requestProcess = $this->getSdk()->{$this->getSdkMethodName()}($this->getRequestObj());

        $result->setSuccess(true)
            ->setResponse($requestProcess->getResponse())
            ->parseFields();

        return $result;
    }

    /**
     * @return Shipment
     */
    public function getShipment(): ?Shipment
    {
        return $this->shipment;
    }

}