<?php


namespace Ipol\Viadelivery\Via\Controller;


use Ipol\Viadelivery\Api\Entity\Request\GetDeliveryInfo;
use Ipol\Viadelivery\Core\Delivery\Shipment;
use Ipol\Viadelivery\Core\Entity\Money;
use Ipol\Viadelivery\Via\AppLevelException;
use Ipol\Viadelivery\Via\Entity\CalculatorResult as ResultObj;

/**
 * Class Calculator
 * @package Ipol\Viadelivery\Via
 * @subpackage Controller
 */
class Calculator extends AutomatedCommonRequest
{
    /**
     * @var Shipment
     */
    protected $shipment;

    /**
     * Calculator constructor.
     * @param ResultObj $resultObj
     * @param Shipment $shipment
     */
    public function __construct(ResultObj $resultObj, Shipment $shipment)
    {
        parent::__construct($resultObj);
        $this->shipment = $shipment;
    }

    /**
     * @return $this
     * @throws AppLevelException
     */
    public function convert(): Calculator
    {
        $shipment = $this->shipment;
        $request = new GetDeliveryInfo();

        $request->setId($this->getSdk()->getUid());
        $dimensions = $shipment->getCargoes()->getTotalDimensions();

        if ($shipment->getTo()->getField('AddressFreeForm')) {
            $request->setAddress($shipment->getTo()->getField('AddressFreeForm'));
        } else {
            $request->setCity($shipment->getTo()->getName())
                ->setRegion($shipment->getTo()->getRegion())
                ->setCountry($shipment->getTo()->getCountry())
                ->setZipCode($shipment->getTo()->getZip());
        }
        if (is_a($shipment->getField('totalOrderCost'), Money::class)) {
            $orderPrice = $shipment->getField('totalOrderCost');
        } else {
            $orderPrice = $shipment->getCargoes()->getTotalPrice();
        }

        $request->setWeight($shipment->getCargoes()->getTotalWeight())
            ->setLength($dimensions['L'] / 10) //Core mm -> API cm
            ->setWidth($dimensions['W'] / 10) //Core mm -> API cm
            ->setHeight($dimensions['H'] / 10) //Core mm -> API cm
            ->setOrderPrice($orderPrice->getAmount())
            ->setPointId($shipment->getPvzIdTo());

        $this->setRequestObj($request);

        return $this;
    }

}