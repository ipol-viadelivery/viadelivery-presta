<?php


namespace Ipol\Viadelivery\Via\Controller;

use Ipol\Viadelivery\Api\Entity\Request\GetPointInfo as PointInfoRequest;
use Ipol\Viadelivery\Via\Entity\PointResult;

class GetPointInfo extends RequestController
{
    public function __construct($pointId)
    {
        $data = new PointInfoRequest();
        $data->setPointId($pointId);
        $this->setRequestObj($data);
    }

    public function execute()
    {
        $data = $this->getRequestObj();
        $data->setId($this->getSdk()->getUid()); //can't set uid in constructor - SDK will be configured only after that

        $result = new PointResult();
        try{
            $request = $this->getSdk()
                ->getPointInfo($data);

            $result->setSuccess(true)
                ->setPointInfo($request->getResponse())
                ->setResponse($request->getResponse())
                ->parseFields();
        }catch(\Exception $e){ //TODO: exception types?
            $result->setSuccess(false)
                ->setError($e->getMessage());
            return false;
        }

        return $result;
    }
}