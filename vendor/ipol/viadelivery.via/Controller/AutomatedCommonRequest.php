<?php


namespace Ipol\Viadelivery\Via\Controller;


use Ipol\Viadelivery\Api\BadResponseException;
use Ipol\Viadelivery\Api\Methods\AbstractMethod;
use Ipol\Viadelivery\Via\AppLevelException;
use Ipol\Viadelivery\Via\Entity\AbstractResult;
use ReflectionClass;
use ReflectionException;

/**
 * Class AutomatedCommonRequest
 * @package Ipol\Viadelivery\Viadelivery
 * @subpackage Controller
 */
class AutomatedCommonRequest extends RequestController
{
    /**
     * @var AbstractResult|mixed
     */
    protected $resultObject;

    /**
     * BasicController constructor.
     * @param AbstractResult|mixed $resultObj
     */
    public function __construct($resultObj)
    {
        $this->resultObject = $resultObj;
    }

    /**
     * @return AbstractResult|mixed
     */
    public function execute()
    {
        $result = $this->getResultObject();

        try {
            if ($this->getRequestObj()) {
                $requestProcess = $this->getSdk()->{$this->getSdkMethodName()}($this->getRequestObj());
            } else {
                $requestProcess = $this->getSdk()->{$this->getSdkMethodName()}();
            }
            /**@var $requestProcess AbstractMethod*/
            $result->setSuccess($requestProcess->getResponse()->getSuccess())
                ->setResponse($requestProcess->getResponse());
            if ($result->isSuccess()) {
                $result->parseFields();
            }
            /*else if(is_a($requestProcess->getResponse(), ErrorResponse::class))
            {
                $result->setError(new ErrorResponseException($requestProcess->getResponse()));
            }*/
        } catch (BadResponseException | AppLevelException $e) {
            $result->setSuccess(false)
                ->setError($e);
        } finally {
            return $result;
        }
    }

    /**
     * @return AbstractResult|mixed
     */
    public function getResultObject()
    {
        return $this->resultObject;
    }

    /**
     * @param AbstractResult|mixed $resultObject
     * @return $this|mixed - mixed for child-classes
     */
    public function setResultObject($resultObject)
    {
        $this->resultObject = $resultObject;
        return $this;
    }

    /**
     * @return string
     */
    public function getSelfHash(): string
    {
        $base = new ReflectionClass(__CLASS__); //classname based on where we declare function - here (AutomatedCommonRequest)
        try {
            $extended = new ReflectionClass(get_class($this));//real running classname - sub-class
            $properties_in = array_diff(
                $extended->getProperties(),
                $base->getProperties()
            );
        } catch (ReflectionException $e) {
            //it's lie, class will always exist, we ARE in it. But for IDE i do this
            $properties_in = [];
        }

        $resString = $this->sdkMethodName; //todo fix problem with no Sdk-method name
        foreach ($properties_in as $property) {
            $resString .= serialize($this->{$property->getName()});
        }
        return md5($resString);
    }

}