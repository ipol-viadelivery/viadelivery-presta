<?php


namespace Ipol\Viadelivery\Via\Controller;


use Ipol\Viadelivery\Api\Entity\Request\GetDeliveryInfoModeDistance;
use Ipol\Viadelivery\Core\Delivery\Shipment;
use Ipol\Viadelivery\Via\AppLevelException;
use Ipol\Viadelivery\Via\Entity\ClosestPointsByAddressResult as ResultObj;

/**
 * Class Calculator
 * @package Ipol\Viadelivery\Via
 * @subpackage Controller
 */
class ClosestPointsByAddressController extends AutomatedCommonRequest
{
    /**
     * @var Shipment
     */
    protected $shipment;

    /**
     * Calculator constructor.
     * @param ResultObj $resultObj
     * @param Shipment $shipment
     * @param string|null $address
     * @param int|null $groupLimit
     */
    public function __construct(ResultObj $resultObj, Shipment $shipment, ?string $address = null, ?int $groupLimit = null)
    {
        parent::__construct($resultObj);
        $this->shipment = $shipment;
        $this->requestObj = new GetDeliveryInfoModeDistance();
        $this->requestObj->setAddress($address)
            ->setGroupLimit($groupLimit);
    }

    /**
     * @return $this
     * @throws AppLevelException
     */
    public function convert(): ClosestPointsByAddressController
    {
        $shipment = $this->shipment;
        $request = $this->requestObj;

        $request->setId($this->getSdk()->getUid());
        $dimensions = $shipment->getCargoes()->getTotalDimensions();

        if (is_null($request->getAddress())) {
            $request->setCity($shipment->getTo()->getName())
                ->setRegion($shipment->getTo()->getRegion())
                ->setCountry($shipment->getTo()->getCountry());
        }

        $request->setWeight($shipment->getCargoes()->getTotalWeight())
            ->setLength($dimensions['L'] / 10) //Core mm -> API cm
            ->setWidth($dimensions['W'] / 10) //Core mm -> API cm
            ->setHeight($dimensions['H'] / 10) //Core mm -> API cm
            ->setOrderPrice($shipment->getCargoes()->getTotalPrice()->getAmount());

        return $this;
    }

}